# jcz-server

#### 介绍
唐山学院17级移动移动开发406宿舍毕业设计
金创展项目后端代码


#### 软件架构

1. 使用SpringBoot作为基础框架
2. ORM框架采用MyBatis3.0
3. 持久化数据库采用MySQL5.5+
4. 缓存数据库采用Redis
5. JDK版本为1.8


#### 安装教程

1.  请确保本地安装Java1.8及以上程序运行环境
2.  开发工具尽量选用IDEA
3.  请确保项目基本运行环境安装：redis 、MySQL5.5以以上 、Gradle4.3级以上
4.  请确保项目的端口号未被占用：11406 , 如需更换端口号，请在resources资源根目录下application下修改端口号

#### 使用说明

1.  请确保本机安装Git环境
2.  在IDEA安装Gitee插件, 在主页面VCS中,选择 Checkout from Version Control -- Git , 输入链接地址即可Clone本项目
3.  本项目依赖于Lombok插件，编译前安装此插件以及配置
4.  请使用Gradle4.3以上项目打包工具运行即可

#### 参与贡献

1.  赵树超
2.  杨帆
3.  张强
4.  刘慧贤
5.  肖晓霖


#### 附页

1.  前端源代码地址：https://gitee.com/zhao_shu_chao/vue_406_manageSystem
2.  Gradle下载地址：https://services.gradle.org/distributions/
3.  Redis下载地址：https://redis.io/download
4.  后台管理系统测试链接：http://producttest.doulaibao.com.cn/products/jcz_manage/#/login
5.  后台管理系统账号密码：均为12345  请勿乱删数据
6.  系统前台测试页面链接：http://producttest.doulaibao.com.cn/products/jcz_front/#/Dashboard
7.  Maven以及Gradel依赖地址：https://mvnrepository.com/
