/*
 Navicat Premium Data Transfer

 Source Server         : 测试
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : 101.201.67.222:3306
 Source Schema         : jcz_product

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 12/05/2020 10:06:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jcz_address
-- ----------------------------
DROP TABLE IF EXISTS `jcz_address`;
CREATE TABLE `jcz_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT 0 COMMENT '用户id',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '收货人所在区域',
  `detailed_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '详细地址',
  `consignee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '收货人',
  `phone` varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '收货人手机号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `delete_flag` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除；0表示未删除，1表示删除',
  `default_flag` int(4) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000 COMMENT '是否为默认地址，0代表不是，1代表是',
  `address_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '地址姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1015 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jcz_address
-- ----------------------------
INSERT INTO `jcz_address` VALUES (1003, 4, '[\"130000\",\"130100\",\"130102\"]', '阿斯顿11', '阿斯顿122', '13121558494', '2020-02-06 19:54:55', '2020-02-06 19:54:55', 0, 0000, '');
INSERT INTO `jcz_address` VALUES (1007, 4, '[\"110000\",\"110100\",\"110101\"]', 'asd', '阿松大1', '18622173897', '2020-02-06 19:32:45', '2020-02-06 19:32:45', 0, 0000, '');
INSERT INTO `jcz_address` VALUES (1008, 4, '[\"120000\",\"120100\",\"120101\"]', '阿斯顿1111', '阿斯顿11123', '13121558494', '2020-02-06 19:52:07', '2020-02-06 19:52:07', 0, 0000, '');
INSERT INTO `jcz_address` VALUES (1009, 1, '[\"110000\",\"110100\",\"110101\"]', '1234', '13244444444', '13244444444', '2020-02-12 17:42:02', '2020-02-12 17:42:02', 0, 0000, '北京市 / 市辖区 / 东城区');
INSERT INTO `jcz_address` VALUES (1010, 1, '[\"130000\",\"130300\",\"130304\"]', '123', '123', '13255555555', '2020-02-12 18:28:20', '2020-02-12 18:28:20', 0, 0001, '河北省 / 秦皇岛市 / 北戴河区');
INSERT INTO `jcz_address` VALUES (1011, 7, '[\"120000\",\"120100\",\"120101\"]', '测试数据详细地址1', '李丽', '15581701653', '2020-03-09 14:36:05', '2020-03-09 14:36:05', 0, 0000, '天津市 / 市辖区 / 和平区');
INSERT INTO `jcz_address` VALUES (1012, 7, '[\"110000\",\"110100\",\"110105\"]', '向阳小区', '马文', '15512342342', '2020-03-09 15:32:08', '2020-03-09 15:32:08', 0, 0001, '北京市 / 市辖区 / 朝阳区');
INSERT INTO `jcz_address` VALUES (1013, 38, '[\"130000\",\"130100\",\"130102\"]', '向阳小区13', '铭铭', '18202261653', '2020-03-12 10:44:07', '2020-03-12 10:44:07', 0, 0001, '河北省 / 石家庄市 / 长安区');
INSERT INTO `jcz_address` VALUES (1014, 39, '[\"120000\",\"120100\",\"120102\"]', '二分法', '大幅度发', '18875767707', '2020-03-20 18:02:12', '2020-03-20 18:02:12', 0, 0001, '天津市 / 市辖区 / 河东区');

-- ----------------------------
-- Table structure for jcz_banner
-- ----------------------------
DROP TABLE IF EXISTS `jcz_banner`;
CREATE TABLE `jcz_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `banner_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `banner_image_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `banner_jump_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '跳转地址',
  `banner_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否显示状态(0-显示；1-不显示)',
  `location` int(11) DEFAULT NULL COMMENT '排序位置',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `delete_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'banner图' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jcz_banner
-- ----------------------------
INSERT INTO `jcz_banner` VALUES (10, '111', 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/331d2855-a837-41c3-b479-47da866a233a_150243.jpg', '123', 0, 0, '2020-03-11 15:02:49', '2020-03-11 15:17:23', 0);
INSERT INTO `jcz_banner` VALUES (11, '2', 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/56c79d83-0d85-4bee-bf3d-6277342e994c_150303.jpg', '4', 1, 2, '2020-03-11 15:03:08', '2020-03-11 15:03:24', 0);
INSERT INTO `jcz_banner` VALUES (12, '123', 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/1e0a30a0-5509-44cd-9b97-5f66e155a5d8_150542.jpg', '444', 1, 3, '2020-03-11 15:05:44', '2020-03-11 15:16:44', 0);
INSERT INTO `jcz_banner` VALUES (13, '4', 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/c469cefc-17f8-4c22-91f0-70c412f3485c_150733.jpg', '123', 1, 123, '2020-03-11 15:07:39', '2020-03-11 15:07:39', 0);
INSERT INTO `jcz_banner` VALUES (14, '123', 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/7587f68f-5cdf-49c9-ba79-45edb5cac992_151123.jpg', '123', 1, 12, '2020-03-11 15:16:18', '2020-03-11 15:16:18', 0);
INSERT INTO `jcz_banner` VALUES (15, '3', 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/fe0ae5c1-4e5c-403f-aa48-7e723f0ebf3b_151629.jpg', '123', 0, 4, '2020-03-11 15:16:33', '2020-03-11 18:44:20', 0);
INSERT INTO `jcz_banner` VALUES (16, '测试添加', 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/514f7190-bea6-48eb-89f3-61f43333f833_184458.jpg', '5', 0, 5, '2020-03-11 18:45:36', '2020-03-11 18:45:42', 0);

-- ----------------------------
-- Table structure for jcz_order
-- ----------------------------
DROP TABLE IF EXISTS `jcz_order`;
CREATE TABLE `jcz_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_number` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单编号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `address_id` int(11) DEFAULT NULL COMMENT '收货地址id',
  `total_count` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '交易状态 0-订单初始化 1-待支付 2-待发货 3-已发货 ',
  `pay_type` tinyint(4) DEFAULT NULL COMMENT '支付方式(1-支付宝；2-银行卡)',
  `total_income` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '实收款',
  `pay_time` datetime(0) DEFAULT NULL COMMENT '支付时间',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `delete_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 否 1 是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jcz_order
-- ----------------------------
INSERT INTO `jcz_order` VALUES (1, NULL, 4, NULL, 2, 0, NULL, 201.72, '2020-02-04 17:47:49', '2020-02-06 16:03:16', '2020-02-06 16:03:16', 0);
INSERT INTO `jcz_order` VALUES (2, NULL, 4, NULL, 1, 0, NULL, 100.86, '2020-02-19 17:47:45', '2020-02-06 16:18:31', '2020-02-06 16:18:31', 0);
INSERT INTO `jcz_order` VALUES (3, NULL, 4, NULL, 1, 0, NULL, 100.86, '2020-02-13 17:47:42', '2020-02-06 17:27:35', '2020-02-06 17:27:35', 0);
INSERT INTO `jcz_order` VALUES (4, NULL, 4, NULL, 1, 0, NULL, 100.86, '2020-02-26 17:47:39', '2020-02-06 17:45:08', '2020-02-06 17:45:08', 0);
INSERT INTO `jcz_order` VALUES (5, NULL, 4, NULL, 1, 0, NULL, 100.86, '2020-02-12 17:48:09', '2020-02-06 18:42:25', '2020-02-06 18:42:25', 0);
INSERT INTO `jcz_order` VALUES (6, NULL, 4, NULL, 1, 0, NULL, 100.86, '2020-02-25 17:48:05', '2020-02-06 18:43:57', '2020-02-06 18:43:57', 0);
INSERT INTO `jcz_order` VALUES (7, NULL, 4, NULL, 2, 0, NULL, 201.72, '2020-02-26 17:47:36', '2020-02-06 20:08:07', '2020-02-06 20:08:07', 0);
INSERT INTO `jcz_order` VALUES (8, 'JCZ202002121704388925553', 1, NULL, 1, 0, NULL, 11.00, '2020-02-04 17:47:52', '2020-02-12 17:04:38', '2020-02-12 17:04:38', 0);
INSERT INTO `jcz_order` VALUES (9, 'JCZ202002121718172122119', 7, NULL, 2, 0, NULL, 66.00, '2020-02-19 17:47:57', '2020-02-12 17:18:17', '2020-02-12 17:18:17', 0);
INSERT INTO `jcz_order` VALUES (10, 'JCZ202002121718532716575', 7, NULL, 2, 0, NULL, 398.00, '2020-02-26 17:47:33', '2020-02-12 17:18:53', '2020-02-12 17:18:53', 0);
INSERT INTO `jcz_order` VALUES (11, 'JCZ202002121720565485536', 7, NULL, 2, 0, NULL, 398.00, '2020-02-19 17:48:02', '2020-02-12 17:20:56', '2020-02-12 17:20:56', 0);
INSERT INTO `jcz_order` VALUES (12, 'JCZ202002121731321078216', 1, NULL, 1, 0, NULL, 11.00, '2020-02-04 17:47:29', '2020-02-12 17:31:32', '2020-02-12 17:31:32', 0);
INSERT INTO `jcz_order` VALUES (13, 'JCZ202002121732501537702', 1, NULL, 1, 0, NULL, 122.00, '2020-02-12 17:47:26', '2020-02-12 17:32:50', '2020-02-12 17:32:50', 0);
INSERT INTO `jcz_order` VALUES (14, 'JCZ202002121741041563051', 1, NULL, 1, 0, NULL, 122.00, '2020-02-12 17:41:04', '2020-02-12 17:41:04', '2020-02-12 17:41:04', 0);
INSERT INTO `jcz_order` VALUES (15, 'JCZ202002121741235531782', 1, NULL, 1, 0, NULL, 111.00, '2020-02-12 17:41:23', '2020-02-12 17:41:23', '2020-02-12 17:41:23', 0);
INSERT INTO `jcz_order` VALUES (16, 'JCZ202002121742208868830', 1, 1009, 1, 3, 1, 110.00, '2020-02-12 17:42:29', '2020-02-12 17:42:20', '2020-02-12 17:42:20', 0);
INSERT INTO `jcz_order` VALUES (17, 'JCZ202002121742569189243', 1, 1009, 1, 3, 1, 123.00, '2020-02-12 17:43:28', '2020-02-12 17:42:56', '2020-02-12 17:42:56', 0);
INSERT INTO `jcz_order` VALUES (18, 'JCZ202002121752057209863', 1, NULL, 1, 0, NULL, 1234.00, '2020-02-12 17:52:05', '2020-02-12 17:52:05', '2020-02-12 17:52:05', 0);
INSERT INTO `jcz_order` VALUES (19, 'JCZ202002121754571899408', 1, NULL, 1, 0, NULL, 1234.00, NULL, '2020-02-12 17:54:57', '2020-02-12 17:54:57', 0);
INSERT INTO `jcz_order` VALUES (20, 'JCZ202002121756257453751', 1, NULL, 1, 0, NULL, 111.00, NULL, '2020-02-12 17:56:25', '2020-02-12 17:56:25', 0);
INSERT INTO `jcz_order` VALUES (21, 'JCZ202002121756572648911', 1, NULL, 2, 0, NULL, 222.00, NULL, '2020-02-12 17:56:57', '2020-02-12 17:56:57', 0);
INSERT INTO `jcz_order` VALUES (22, 'JCZ202002121819396943742', 1, NULL, 1, 0, NULL, 111.00, NULL, '2020-02-12 18:19:39', '2020-02-12 18:19:39', 0);
INSERT INTO `jcz_order` VALUES (23, 'JCZ202002121819531986311', 1, NULL, 1, 0, NULL, 111.00, NULL, '2020-02-12 18:19:53', '2020-02-12 18:19:53', 0);
INSERT INTO `jcz_order` VALUES (24, 'JCZ202002121821102185712', 1, NULL, 1, 0, NULL, 111.00, NULL, '2020-02-12 18:21:10', '2020-02-12 18:21:10', 0);
INSERT INTO `jcz_order` VALUES (81, 'JCZ2020021217565726343434', 1, 1099, 0, 0, NULL, 0.00, '2020-02-18 18:23:10', '2020-02-12 18:22:30', '2020-02-12 18:22:30', 0);
INSERT INTO `jcz_order` VALUES (106, 'JCZ202002121825069707864', 1, 1009, 1, 2, 1, 111.00, '2020-02-12 18:25:24', '2020-02-12 18:25:06', '2020-02-12 18:25:06', 0);
INSERT INTO `jcz_order` VALUES (107, 'JCZ202002121825395023089', 1, NULL, 1, 0, NULL, 111.00, NULL, '2020-02-12 18:25:39', '2020-02-12 18:25:39', 0);
INSERT INTO `jcz_order` VALUES (108, 'JCZ202002121825489549982', 1, 1009, 7, 3, 1, 777.00, '2020-02-12 18:25:52', '2020-02-12 18:25:48', '2020-02-12 18:25:48', 0);
INSERT INTO `jcz_order` VALUES (109, 'JCZ202002121827141323317', 1, 1009, 1, 0, NULL, 11.00, NULL, '2020-02-12 18:27:14', '2020-02-12 18:27:14', 0);
INSERT INTO `jcz_order` VALUES (110, 'JCZ202002121827348991171', 1, 1010, 1, 2, 1, 11.00, '2020-02-12 18:27:38', '2020-02-12 18:27:34', '2020-02-12 18:27:34', 0);
INSERT INTO `jcz_order` VALUES (111, 'JCZ202002121906337487143', 1, NULL, 1, 0, NULL, 111.00, NULL, '2020-02-12 19:06:33', '2020-02-12 19:06:33', 0);
INSERT INTO `jcz_order` VALUES (112, 'JCZ202003091435092110359', 7, 1011, 2, 3, 1, 1000.00, '2020-03-09 14:37:22', '2020-03-09 14:35:09', '2020-03-09 14:35:09', 0);
INSERT INTO `jcz_order` VALUES (113, 'JCZ202003091531112066670', 7, 1012, 3, 2, 2, 27.00, '2020-03-09 15:33:12', '2020-03-09 15:31:11', '2020-03-09 15:31:11', 0);
INSERT INTO `jcz_order` VALUES (114, 'JCZ202003091553125012418', 7, 1012, 1, 2, 1, 499.90, '2020-03-09 15:53:21', '2020-03-09 15:53:12', '2020-03-09 15:53:12', 0);
INSERT INTO `jcz_order` VALUES (117, 'JCZ202003111820373324028', 7, 1012, 1, 2, 1, 499.90, '2020-03-11 18:21:38', '2020-03-11 18:20:37', '2020-03-11 18:20:37', 0);
INSERT INTO `jcz_order` VALUES (118, 'JCZ202003111827049172364', 7, 1012, 1, 3, 1, 121.00, '2020-03-11 18:27:16', '2020-03-11 18:27:04', '2020-03-11 18:27:04', 0);
INSERT INTO `jcz_order` VALUES (119, 'JCZ202003111838495207009', 7, 1012, 2, 3, 2, 1000.00, '2020-03-11 18:39:17', '2020-03-11 18:38:49', '2020-03-11 18:38:49', 0);
INSERT INTO `jcz_order` VALUES (120, 'JCZ202003111908014991714', 7, 1012, 1, 0, NULL, 11.00, NULL, '2020-03-11 19:08:01', '2020-03-11 19:08:01', 0);
INSERT INTO `jcz_order` VALUES (121, 'JCZ202003111938087741768', 7, 1011, 1, 2, 1, 122.00, '2020-03-11 19:38:20', '2020-03-11 19:38:08', '2020-03-11 19:38:08', 0);
INSERT INTO `jcz_order` VALUES (122, 'JCZ202003112024353096146', 7, 1012, 1, 0, NULL, 499.90, NULL, '2020-03-11 20:24:35', '2020-03-11 20:24:35', 0);
INSERT INTO `jcz_order` VALUES (123, 'JCZ202003112026093446272', 7, 1012, 1, 0, NULL, 11.00, NULL, '2020-03-11 20:26:09', '2020-03-11 20:26:09', 0);
INSERT INTO `jcz_order` VALUES (124, 'JCZ202003112037006962188', 7, 1012, 1, 2, 1, 122.00, '2020-03-11 20:37:07', '2020-03-11 20:37:00', '2020-03-11 20:37:00', 0);
INSERT INTO `jcz_order` VALUES (125, 'JCZ202003112055230055175', 7, 1012, 1, 0, NULL, 100.00, NULL, '2020-03-11 20:55:23', '2020-03-11 20:55:23', 0);
INSERT INTO `jcz_order` VALUES (126, 'JCZ202003112058069767108', 7, NULL, 1, 0, NULL, 100.00, NULL, '2020-03-11 20:58:06', '2020-03-11 20:58:06', 0);
INSERT INTO `jcz_order` VALUES (127, 'JCZ202003121039072654933', 7, NULL, 1, 0, NULL, 11.00, NULL, '2020-03-12 10:39:07', '2020-03-12 10:39:07', 0);
INSERT INTO `jcz_order` VALUES (128, 'JCZ202003121039489904642', 7, NULL, 1, 0, NULL, 11.00, NULL, '2020-03-12 10:39:48', '2020-03-12 10:39:48', 0);
INSERT INTO `jcz_order` VALUES (129, 'JCZ202003121041010156110', 7, 1012, 1, 0, NULL, 500.00, NULL, '2020-03-12 10:41:01', '2020-03-12 10:41:01', 0);
INSERT INTO `jcz_order` VALUES (130, 'JCZ202003121041499729799', 7, 1012, 1, 2, 1, 500.00, '2020-03-12 10:46:03', '2020-03-12 10:41:49', '2020-03-12 10:41:49', 0);
INSERT INTO `jcz_order` VALUES (131, 'JCZ202003121043014776816', 38, NULL, 1, 0, NULL, 121.00, NULL, '2020-03-12 10:43:01', '2020-03-12 10:43:01', 0);
INSERT INTO `jcz_order` VALUES (132, 'JCZ202003121223059552162', 7, 1012, 1, 2, 1, 100.00, '2020-03-12 12:23:28', '2020-03-12 12:23:05', '2020-03-12 12:23:05', 0);
INSERT INTO `jcz_order` VALUES (133, 'JCZ202003121225411460162', 7, 1012, 1, 2, 1, 500.00, '2020-03-12 12:27:15', '2020-03-12 12:25:41', '2020-03-12 12:25:41', 0);
INSERT INTO `jcz_order` VALUES (134, 'JCZ202003121231248388153', 7, 1012, 1, 2, 1, 499.90, '2020-03-12 12:31:31', '2020-03-12 12:31:24', '2020-03-12 12:31:24', 0);
INSERT INTO `jcz_order` VALUES (135, 'JCZ202003121539269328448', 7, 1012, 1, 2, 1, 121.00, '2020-03-12 15:39:34', '2020-03-12 15:39:26', '2020-03-12 15:39:26', 0);
INSERT INTO `jcz_order` VALUES (136, 'JCZ202003201801040529775', 39, NULL, 1, 0, NULL, 122.00, NULL, '2020-03-20 18:01:04', '2020-03-20 18:01:04', 0);

-- ----------------------------
-- Table structure for jcz_order_product
-- ----------------------------
DROP TABLE IF EXISTS `jcz_order_product`;
CREATE TABLE `jcz_order_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_id` int(11) DEFAULT NULL COMMENT '产品id',
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `income` decimal(10, 2) DEFAULT NULL COMMENT '实收款',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `delete_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除: 0 否 1 是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 133 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单产品关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jcz_order_product
-- ----------------------------
INSERT INTO `jcz_order_product` VALUES (1, 1, 1, NULL, NULL, '2020-02-05 10:19:21', '2020-02-05 10:19:21', 0);
INSERT INTO `jcz_order_product` VALUES (5, 2, 2, NULL, NULL, '2020-02-05 10:19:45', '2020-02-05 10:19:45', 1);
INSERT INTO `jcz_order_product` VALUES (7, 1, 3, NULL, NULL, '2020-02-05 10:20:53', '2020-02-05 10:20:53', 0);
INSERT INTO `jcz_order_product` VALUES (9, 2, 5, 3, 302.46, '2020-02-06 20:12:50', '2020-02-06 20:12:50', 0);
INSERT INTO `jcz_order_product` VALUES (10, 1, 6, 1, 100.81, '2020-02-06 21:41:34', '2020-02-06 21:41:34', 0);
INSERT INTO `jcz_order_product` VALUES (11, 7, 7, 1, 122.00, '2020-02-06 23:20:55', '2020-02-06 23:20:55', 0);
INSERT INTO `jcz_order_product` VALUES (12, 3, 8, 1, 100.83, '2020-02-06 23:32:45', '2020-02-06 23:32:45', 0);
INSERT INTO `jcz_order_product` VALUES (13, 9, 9, 1, 11.00, '2020-02-10 09:23:40', '2020-02-10 09:23:40', 0);
INSERT INTO `jcz_order_product` VALUES (14, 8, 10, 1, 11.00, '2020-02-11 09:55:47', '2020-02-11 09:55:47', 0);
INSERT INTO `jcz_order_product` VALUES (15, 6, 11, 1, 122.00, '2020-02-11 10:06:58', '2020-02-11 10:06:58', 0);
INSERT INTO `jcz_order_product` VALUES (16, 8, 12, 2, 22.00, '2020-02-11 10:27:09', '2020-02-11 10:27:09', 0);
INSERT INTO `jcz_order_product` VALUES (17, 6, 13, 1, 122.00, '2020-02-11 10:28:55', '2020-02-11 10:28:55', 0);
INSERT INTO `jcz_order_product` VALUES (18, 8, 14, 1, 11.00, '2020-02-11 10:35:03', '2020-02-11 10:35:03', 0);
INSERT INTO `jcz_order_product` VALUES (19, 6, 15, 1, 122.00, '2020-02-11 10:36:12', '2020-02-11 10:36:12', 0);
INSERT INTO `jcz_order_product` VALUES (20, 8, 16, 1, 11.00, '2020-02-11 10:37:01', '2020-02-11 10:37:01', 0);
INSERT INTO `jcz_order_product` VALUES (21, 8, 17, 1, 11.00, '2020-02-11 10:42:24', '2020-02-11 10:42:24', 0);
INSERT INTO `jcz_order_product` VALUES (22, 6, 18, 1, 122.00, '2020-02-11 10:56:18', '2020-02-11 10:56:18', 0);
INSERT INTO `jcz_order_product` VALUES (23, 5, 19, 1, 100.85, '2020-02-11 11:02:32', '2020-02-11 11:02:32', 0);
INSERT INTO `jcz_order_product` VALUES (24, 6, 20, 1, 122.00, '2020-02-11 11:03:08', '2020-02-11 11:03:08', 0);
INSERT INTO `jcz_order_product` VALUES (25, 8, 21, 1, 11.00, '2020-02-11 11:05:17', '2020-02-11 11:05:17', 0);
INSERT INTO `jcz_order_product` VALUES (26, 8, 22, 3, 33.00, '2020-02-11 11:12:20', '2020-02-11 11:12:20', 0);
INSERT INTO `jcz_order_product` VALUES (27, 6, 23, 1, 122.00, '2020-02-11 11:13:06', '2020-02-11 11:13:06', 0);
INSERT INTO `jcz_order_product` VALUES (28, 6, 24, 1, 122.00, '2020-02-11 11:28:44', '2020-02-11 11:28:44', 0);
INSERT INTO `jcz_order_product` VALUES (29, 8, 25, 1, 11.00, '2020-02-11 11:34:07', '2020-02-11 11:34:07', 0);
INSERT INTO `jcz_order_product` VALUES (30, 12, 26, 1, 121.00, '2020-02-11 11:36:24', '2020-02-11 11:36:24', 0);
INSERT INTO `jcz_order_product` VALUES (31, 15, 27, 1, 500.00, '2020-02-11 12:08:37', '2020-02-11 12:08:37', 0);
INSERT INTO `jcz_order_product` VALUES (32, 8, 28, 1, 11.00, '2020-02-11 12:09:23', '2020-02-11 12:09:23', 0);
INSERT INTO `jcz_order_product` VALUES (33, 8, 29, 1, 11.00, '2020-02-11 12:09:41', '2020-02-11 12:09:41', 0);
INSERT INTO `jcz_order_product` VALUES (34, 8, 30, 1, 11.00, '2020-02-11 12:09:50', '2020-02-11 12:09:50', 0);
INSERT INTO `jcz_order_product` VALUES (35, 8, 31, 1, 11.00, '2020-02-11 12:10:22', '2020-02-11 12:10:22', 0);
INSERT INTO `jcz_order_product` VALUES (36, 8, 32, 1, 11.00, '2020-02-11 12:10:58', '2020-02-11 12:10:58', 0);
INSERT INTO `jcz_order_product` VALUES (37, 6, 33, 1, 122.00, '2020-02-11 12:31:44', '2020-02-11 12:31:44', 0);
INSERT INTO `jcz_order_product` VALUES (38, 8, 34, 1, 11.00, '2020-02-11 12:32:14', '2020-02-11 12:32:14', 0);
INSERT INTO `jcz_order_product` VALUES (39, 6, 35, 1, 122.00, '2020-02-11 14:11:44', '2020-02-11 14:11:44', 0);
INSERT INTO `jcz_order_product` VALUES (40, 6, 36, 2, 244.00, '2020-02-11 14:11:58', '2020-02-11 14:11:58', 0);
INSERT INTO `jcz_order_product` VALUES (41, 13, 37, 1, 111.00, '2020-02-11 14:16:53', '2020-02-11 14:16:53', 0);
INSERT INTO `jcz_order_product` VALUES (42, 6, 38, 1, 122.00, '2020-02-11 14:17:57', '2020-02-11 14:17:57', 0);
INSERT INTO `jcz_order_product` VALUES (43, 6, 39, 1, 122.00, '2020-02-11 14:24:52', '2020-02-11 14:24:52', 0);
INSERT INTO `jcz_order_product` VALUES (44, 8, 40, 1, 11.00, '2020-02-11 14:34:12', '2020-02-11 14:34:12', 0);
INSERT INTO `jcz_order_product` VALUES (45, 6, 41, 5, 610.00, '2020-02-11 14:43:56', '2020-02-11 14:43:56', 0);
INSERT INTO `jcz_order_product` VALUES (46, 6, 42, 1, 122.00, '2020-02-11 14:53:23', '2020-02-11 14:53:23', 0);
INSERT INTO `jcz_order_product` VALUES (47, 5, 43, 1, 100.85, '2020-02-11 14:53:41', '2020-02-11 14:53:41', 0);
INSERT INTO `jcz_order_product` VALUES (48, 6, 44, 1, 122.00, '2020-02-11 14:54:29', '2020-02-11 14:54:29', 0);
INSERT INTO `jcz_order_product` VALUES (49, 15, 45, 1, 499.90, '2020-02-11 14:58:36', '2020-02-11 14:58:36', 0);
INSERT INTO `jcz_order_product` VALUES (50, 10, 46, 1, 11.00, '2020-02-11 15:00:38', '2020-02-11 15:00:38', 0);
INSERT INTO `jcz_order_product` VALUES (51, 11, 47, 1, 121.00, '2020-02-11 15:02:54', '2020-02-11 15:02:54', 0);
INSERT INTO `jcz_order_product` VALUES (52, 24, 48, 1, 9999.00, '2020-02-11 15:05:21', '2020-02-11 15:05:21', 0);
INSERT INTO `jcz_order_product` VALUES (53, 15, 49, 1, 499.90, '2020-02-11 15:05:33', '2020-02-11 15:05:33', 0);
INSERT INTO `jcz_order_product` VALUES (54, 10, 50, 1, 11.00, '2020-02-11 15:07:09', '2020-02-11 15:07:09', 0);
INSERT INTO `jcz_order_product` VALUES (55, 10, 51, 1, 11.00, '2020-02-11 15:08:15', '2020-02-11 15:08:15', 0);
INSERT INTO `jcz_order_product` VALUES (56, 15, 52, 1, 499.90, '2020-02-11 15:09:01', '2020-02-11 15:09:01', 0);
INSERT INTO `jcz_order_product` VALUES (57, 24, 53, 1, 9999.00, '2020-02-11 15:11:11', '2020-02-11 15:11:11', 0);
INSERT INTO `jcz_order_product` VALUES (58, 10, 54, 1, 11.00, '2020-02-11 15:22:47', '2020-02-11 15:22:47', 0);
INSERT INTO `jcz_order_product` VALUES (59, 10, 55, 1, 11.00, '2020-02-11 15:38:51', '2020-02-11 15:38:51', 0);
INSERT INTO `jcz_order_product` VALUES (60, 15, 56, 1, 499.90, '2020-02-11 15:48:11', '2020-02-11 15:48:11', 0);
INSERT INTO `jcz_order_product` VALUES (61, 10, 57, 1, 11.00, '2020-02-11 15:48:59', '2020-02-11 15:48:59', 0);
INSERT INTO `jcz_order_product` VALUES (62, 10, 58, 1, 11.00, '2020-02-11 15:50:46', '2020-02-11 15:50:46', 0);
INSERT INTO `jcz_order_product` VALUES (63, 15, 59, 1, 499.90, '2020-02-11 16:06:24', '2020-02-11 16:06:24', 0);
INSERT INTO `jcz_order_product` VALUES (64, 15, 60, 1, 499.90, '2020-02-11 16:15:14', '2020-02-11 16:15:14', 0);
INSERT INTO `jcz_order_product` VALUES (65, 26, 61, 1, 99.00, '2020-02-11 16:31:13', '2020-02-11 16:31:13', 0);
INSERT INTO `jcz_order_product` VALUES (66, 10, 62, 1, 11.00, '2020-02-11 16:32:05', '2020-02-11 16:32:05', 0);
INSERT INTO `jcz_order_product` VALUES (67, 26, 63, 12, 1188.00, '2020-02-11 16:33:24', '2020-02-11 16:33:24', 0);
INSERT INTO `jcz_order_product` VALUES (68, 1, 64, 1, 100.81, '2020-02-11 16:56:38', '2020-02-11 16:56:38', 0);
INSERT INTO `jcz_order_product` VALUES (69, 10, 65, 1, 11.00, '2020-02-11 17:14:59', '2020-02-11 17:14:59', 0);
INSERT INTO `jcz_order_product` VALUES (70, 24, 66, 1, 9999.00, '2020-02-11 17:17:17', '2020-02-11 17:17:17', 0);
INSERT INTO `jcz_order_product` VALUES (71, 8, 67, 1, 11.00, '2020-02-11 17:20:38', '2020-02-11 17:20:38', 0);
INSERT INTO `jcz_order_product` VALUES (72, 24, 68, 1, 9999.00, '2020-02-11 17:37:00', '2020-02-11 17:37:00', 0);
INSERT INTO `jcz_order_product` VALUES (73, 15, 69, 1, 499.90, '2020-02-11 17:42:24', '2020-02-11 17:42:24', 0);
INSERT INTO `jcz_order_product` VALUES (74, 26, 70, 2, 198.00, '2020-02-11 18:00:02', '2020-02-11 18:00:02', 0);
INSERT INTO `jcz_order_product` VALUES (75, 26, 71, 1, 99.00, '2020-02-11 18:05:46', '2020-02-11 18:05:46', 0);
INSERT INTO `jcz_order_product` VALUES (76, 15, 72, 1, 499.90, '2020-02-11 18:06:32', '2020-02-11 18:06:32', 0);
INSERT INTO `jcz_order_product` VALUES (77, 15, 73, 2, 999.80, '2020-02-11 18:08:03', '2020-02-11 18:08:03', 0);
INSERT INTO `jcz_order_product` VALUES (78, 8, 74, 1, 11.00, '2020-02-12 09:30:43', '2020-02-12 09:30:43', 0);
INSERT INTO `jcz_order_product` VALUES (79, 24, 75, 1, 9999.00, '2020-02-12 10:28:03', '2020-02-12 10:28:03', 0);
INSERT INTO `jcz_order_product` VALUES (80, 10, 76, 1, 11.00, '2020-02-12 10:54:40', '2020-02-12 10:54:40', 0);
INSERT INTO `jcz_order_product` VALUES (81, 25, 77, 3, 99.00, '2020-02-12 11:22:40', '2020-02-12 11:22:40', 0);
INSERT INTO `jcz_order_product` VALUES (82, 10, 78, 2, 22.00, '2020-02-12 14:27:01', '2020-02-12 14:27:01', 0);
INSERT INTO `jcz_order_product` VALUES (83, 15, 79, 1, 499.90, '2020-02-12 14:31:32', '2020-02-12 14:31:32', 0);
INSERT INTO `jcz_order_product` VALUES (103, 29, 106, 1, 111.00, '2020-02-12 18:25:06', '2020-02-12 18:25:06', 0);
INSERT INTO `jcz_order_product` VALUES (104, 29, 107, 1, 111.00, '2020-02-12 18:25:39', '2020-02-12 18:25:39', 0);
INSERT INTO `jcz_order_product` VALUES (105, 29, 108, 7, 777.00, '2020-02-12 18:25:48', '2020-02-12 18:25:48', 0);
INSERT INTO `jcz_order_product` VALUES (106, 10, 109, 1, 11.00, '2020-02-12 18:27:14', '2020-02-12 18:27:14', 0);
INSERT INTO `jcz_order_product` VALUES (107, 10, 110, 1, 11.00, '2020-02-12 18:27:34', '2020-02-12 18:27:34', 0);
INSERT INTO `jcz_order_product` VALUES (108, 29, 111, 1, 111.00, '2020-02-12 19:06:33', '2020-02-12 19:06:33', 0);
INSERT INTO `jcz_order_product` VALUES (109, 16, 112, 2, 1000.00, '2020-03-09 14:35:09', '2020-03-09 14:35:09', 0);
INSERT INTO `jcz_order_product` VALUES (110, 26, 113, 3, 27.00, '2020-03-09 15:31:11', '2020-03-09 15:31:11', 0);
INSERT INTO `jcz_order_product` VALUES (111, 15, 114, 1, 499.90, '2020-03-09 15:53:12', '2020-03-09 15:53:12', 0);
INSERT INTO `jcz_order_product` VALUES (112, 8, 115, 1, 11.00, '2020-03-11 17:02:38', '2020-03-11 17:02:38', 0);
INSERT INTO `jcz_order_product` VALUES (113, 15, 117, 1, 499.90, '2020-03-11 18:20:37', '2020-03-11 18:20:37', 0);
INSERT INTO `jcz_order_product` VALUES (114, 11, 118, 1, 121.00, '2020-03-11 18:27:04', '2020-03-11 18:27:04', 0);
INSERT INTO `jcz_order_product` VALUES (115, 16, 119, 2, 1000.00, '2020-03-11 18:38:49', '2020-03-11 18:38:49', 0);
INSERT INTO `jcz_order_product` VALUES (116, 10, 120, 1, 11.00, '2020-03-11 19:08:01', '2020-03-11 19:08:01', 0);
INSERT INTO `jcz_order_product` VALUES (117, 6, 121, 1, 122.00, '2020-03-11 19:38:08', '2020-03-11 19:38:08', 0);
INSERT INTO `jcz_order_product` VALUES (118, 15, 122, 1, 499.90, '2020-03-11 20:24:35', '2020-03-11 20:24:35', 0);
INSERT INTO `jcz_order_product` VALUES (119, 10, 123, 1, 11.00, '2020-03-11 20:26:09', '2020-03-11 20:26:09', 0);
INSERT INTO `jcz_order_product` VALUES (120, 6, 124, 1, 122.00, '2020-03-11 20:37:00', '2020-03-11 20:37:00', 0);
INSERT INTO `jcz_order_product` VALUES (121, 14, 125, 1, 100.00, '2020-03-11 20:55:23', '2020-03-11 20:55:23', 0);
INSERT INTO `jcz_order_product` VALUES (122, 14, 126, 1, 100.00, '2020-03-11 20:58:06', '2020-03-11 20:58:06', 0);
INSERT INTO `jcz_order_product` VALUES (123, 10, 127, 1, 11.00, '2020-03-12 10:39:07', '2020-03-12 10:39:07', 0);
INSERT INTO `jcz_order_product` VALUES (124, 10, 128, 1, 11.00, '2020-03-12 10:39:48', '2020-03-12 10:39:48', 0);
INSERT INTO `jcz_order_product` VALUES (125, 16, 129, 1, 500.00, '2020-03-12 10:41:01', '2020-03-12 10:41:01', 0);
INSERT INTO `jcz_order_product` VALUES (126, 16, 130, 1, 500.00, '2020-03-12 10:41:49', '2020-03-12 10:41:49', 0);
INSERT INTO `jcz_order_product` VALUES (127, 11, 131, 1, 121.00, '2020-03-12 10:43:01', '2020-03-12 10:43:01', 0);
INSERT INTO `jcz_order_product` VALUES (128, 14, 132, 1, 100.00, '2020-03-12 12:23:05', '2020-03-12 12:23:05', 0);
INSERT INTO `jcz_order_product` VALUES (129, 16, 133, 1, 500.00, '2020-03-12 12:25:41', '2020-03-12 12:25:41', 0);
INSERT INTO `jcz_order_product` VALUES (130, 15, 134, 1, 499.90, '2020-03-12 12:31:24', '2020-03-12 12:31:24', 0);
INSERT INTO `jcz_order_product` VALUES (131, 11, 135, 1, 121.00, '2020-03-12 15:39:26', '2020-03-12 15:39:26', 0);
INSERT INTO `jcz_order_product` VALUES (132, 6, 136, 1, 122.00, '2020-03-20 18:01:04', '2020-03-20 18:01:04', 0);

-- ----------------------------
-- Table structure for jcz_product
-- ----------------------------
DROP TABLE IF EXISTS `jcz_product`;
CREATE TABLE `jcz_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品名称',
  `describe` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '商品描述',
  `price` decimal(10, 2) DEFAULT NULL COMMENT '单价',
  `brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '品牌',
  `product_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类别',
  `upper_shelf_status` tinyint(4) DEFAULT 0 COMMENT '上下架状态，0表示上架，1表示下架',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `delete_falg` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除；0表示未删除，1表示删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jcz_product
-- ----------------------------
INSERT INTO `jcz_product` VALUES (1, '商品1', 'xxxxx123', 100.81, '品牌1', '化工', 0, '2020-02-06 23:27:16', '2020-02-06 23:27:16', 0);
INSERT INTO `jcz_product` VALUES (2, '商品2', 'xxxxx', 100.82, '品牌2', '化工', 1, '2020-02-05 20:45:20', '2020-02-05 20:45:20', 0);
INSERT INTO `jcz_product` VALUES (3, '商品3', 'xxxxx', 100.83, '品牌3', '化工', 1, '2020-02-05 20:45:20', '2020-02-05 20:45:20', 0);
INSERT INTO `jcz_product` VALUES (4, '商品4', 'xxxxx', 100.84, '品牌4', '化工', 1, '2020-02-05 20:45:20', '2020-02-05 20:45:20', 0);
INSERT INTO `jcz_product` VALUES (5, '商品5', 'xxxxx', 100.85, '品牌5', '化工', 1, '2020-02-05 20:45:20', '2020-02-05 20:45:20', 0);
INSERT INTO `jcz_product` VALUES (6, '122', '32323232', 122.00, '测试品牌', '测试分类', 0, '2020-02-06 18:49:10', '2020-02-06 18:49:10', 0);
INSERT INTO `jcz_product` VALUES (7, '122', '1111', 122.00, '测试品牌', '测试分类', 1, '2020-02-11 11:05:48', '2020-02-11 11:05:48', 0);
INSERT INTO `jcz_product` VALUES (8, '测试111', '111', 11.00, '测试11', '测试11', 0, '2020-02-11 11:05:45', '2020-02-11 11:05:45', 0);
INSERT INTO `jcz_product` VALUES (9, '测试111', '111', 11.00, '测试11', '测试11', 1, '2020-02-11 11:05:43', '2020-02-11 11:05:43', 0);
INSERT INTO `jcz_product` VALUES (10, '11', '111', 11.00, '11', '11', 0, '2020-02-11 11:05:42', '2020-02-11 11:05:42', 0);
INSERT INTO `jcz_product` VALUES (11, '1212', '11', 121.00, '121212', '12121', 0, '2020-02-11 11:05:41', '2020-02-11 11:05:41', 0);
INSERT INTO `jcz_product` VALUES (12, '1212', '1', 121.00, '121212', '12121', 0, '2020-02-11 11:05:39', '2020-02-11 11:05:39', 0);
INSERT INTO `jcz_product` VALUES (13, '111', '测试商品描述111', 111.00, '111', '111', 0, '2020-02-06 15:44:23', '2020-02-06 15:44:23', 0);
INSERT INTO `jcz_product` VALUES (14, 'POS机', '索尼收银POS机', 100.00, '索尼', '收银', 0, '2020-02-10 15:05:56', '2020-02-10 15:05:56', 0);
INSERT INTO `jcz_product` VALUES (15, '测试pos机', '这里应该是编辑器这里应该是编辑器这里应该是编辑器这里应该是编辑器这里应该是编辑器这里应该是编辑器这里应该是编辑器这里应该是编辑器这里应该是编辑器这里应该是编辑器', 499.90, '测试品牌', '分类测试1', 0, '2020-02-11 14:15:48', '2020-02-11 14:15:48', 0);
INSERT INTO `jcz_product` VALUES (16, 'pos机测试1', '描述而测试刷卡pose机个人收款机宝手机专用卡拉卡机小型银联ps机信用卡机器poss收银机移动收银纸全国通用 2020新款深蓝色刷卡pose机个人收款机宝手机专用卡拉卡机小型银联ps机信用卡机器poss收银机移动收银纸全国通用 2020新款深蓝色刷卡pose机个人收款机宝手机专用卡拉卡机小型银联ps机信用卡机器poss收银机移动收银纸全国通用 2020新款深蓝色刷卡pose机个人收款机宝手机专用卡拉卡机小型银联ps机信用卡机器poss收银机移动收银纸全国通用 2020新款深蓝色', 500.00, '测试品牌', '测试分类', 0, '2020-02-10 16:31:44', '2020-02-10 16:31:44', 0);
INSERT INTO `jcz_product` VALUES (17, '曹晓敏测试商品', '最美的玫瑰', 198.00, '红椒', '生活用品', 0, '2020-02-11 09:50:45', '2020-02-11 09:50:45', 0);
INSERT INTO `jcz_product` VALUES (18, 'ads232d', '1111', 21.00, 'csdf323sb', 'df23dc', 0, '2020-02-11 11:05:38', '2020-02-11 11:05:38', 0);
INSERT INTO `jcz_product` VALUES (19, '标题', '秒谁', 122.00, '1212', '1212', 0, '2020-02-11 10:44:31', '2020-02-11 10:44:31', 0);
INSERT INTO `jcz_product` VALUES (20, 'ads232d', '1', 21.00, 'csdf323sb', 'df23dc', 0, '2020-02-11 11:05:36', '2020-02-11 11:05:36', 0);
INSERT INTO `jcz_product` VALUES (21, '!11', '111', 21.00, '123', 'as', 0, '2020-02-11 11:05:35', '2020-02-11 11:05:35', 0);
INSERT INTO `jcz_product` VALUES (22, '!11', NULL, 21.00, '123', 'as', 0, '2020-02-11 11:06:09', '2020-02-11 11:06:09', 0);
INSERT INTO `jcz_product` VALUES (23, '商品测试曹', '描述测试', 199.00, '花火', '生活', 0, '2020-02-11 14:42:11', '2020-02-11 14:42:11', 0);
INSERT INTO `jcz_product` VALUES (24, '金箍棒', '商品测试描述个梵蒂冈梵蒂冈电饭锅的非官方的给对方是个丰东股份的事股份第三个人豆腐干豆腐干豆腐割发代首刚发的感谢发短信割发代首个梵蒂冈的非官方的广东广东分公司股份个富', 9999.00, '无', '武器', 0, '2020-02-11 17:36:28', '2020-02-11 17:36:28', 0);
INSERT INTO `jcz_product` VALUES (25, '草草', '小草生长野蛮', 33.00, '方洁', '卫生', 0, '2020-02-11 15:04:32', '2020-02-11 15:04:32', 0);
INSERT INTO `jcz_product` VALUES (26, 'POS机测试产品2', '产品描述测试文案，详情的编辑在哪里', 9.00, '品牌测试', '分类测试', 0, '2020-02-12 17:51:13', '2020-02-12 17:51:13', 0);
INSERT INTO `jcz_product` VALUES (27, '缓存信息测试', '发布商品缓存信息测试', 12.00, '测试版', '测试', 0, '2020-03-11 15:33:59', '2020-03-11 15:33:59', 0);
INSERT INTO `jcz_product` VALUES (28, '123', '既合乎丰虎航说的话反馈', 1234.00, '123', '1234', 0, '2020-02-12 17:49:46', '2020-02-12 17:49:46', 0);
INSERT INTO `jcz_product` VALUES (29, '111111', '12333333', 111.00, '11', '11', 0, '2020-02-12 17:40:53', '2020-02-12 17:40:53', 0);
INSERT INTO `jcz_product` VALUES (30, '大幅度发的', '王菲菲我分为非我方范德萨范德萨发', 111.00, '完全物权法v', '问我', 0, '2020-02-12 18:00:43', '2020-02-12 18:00:43', 0);
INSERT INTO `jcz_product` VALUES (31, '!11', NULL, 21.00, '123', 'as', 0, '2020-02-12 18:04:34', '2020-02-12 18:04:34', 0);
INSERT INTO `jcz_product` VALUES (32, '12', '描述', 2.00, '11', '11', 0, '2020-03-11 15:20:40', '2020-03-11 15:20:40', 0);
INSERT INTO `jcz_product` VALUES (33, '商品标题', '没有描述', 11.00, '商品分来', '商品分来', 0, '2020-03-11 15:25:05', '2020-03-11 15:25:05', 0);

-- ----------------------------
-- Table structure for jcz_product_picture
-- ----------------------------
DROP TABLE IF EXISTS `jcz_product_picture`;
CREATE TABLE `jcz_product_picture`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '图片id',
  `product_id` int(11) DEFAULT NULL COMMENT '产品id',
  `picture_url` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '图片地址',
  `is_show` tinyint(4) DEFAULT 1 COMMENT '订单显示图(0-显示；1-不显示)',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `delete_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除: 0 否 1 是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 129 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品图片' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jcz_product_picture
-- ----------------------------
INSERT INTO `jcz_product_picture` VALUES (2, 2, 'url2', 0, '2020-02-05 10:13:09', '2020-02-05 10:13:09', 0);
INSERT INTO `jcz_product_picture` VALUES (3, 3, 'url3', 0, '2020-02-05 10:13:09', '2020-02-05 10:13:09', 0);
INSERT INTO `jcz_product_picture` VALUES (4, 4, 'url4', 0, '2020-02-05 10:13:09', '2020-02-05 10:13:09', 0);
INSERT INTO `jcz_product_picture` VALUES (5, 5, 'url5', 0, '2020-02-05 10:13:09', '2020-02-05 10:13:09', 0);
INSERT INTO `jcz_product_picture` VALUES (7, 6, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/31d9e70f-4bb5-479a-9200-1c2c7f6c15c4_145534.jpg', 0, '2020-02-06 14:55:38', '2020-02-06 14:55:38', 0);
INSERT INTO `jcz_product_picture` VALUES (8, 7, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/31d9e70f-4bb5-479a-9200-1c2c7f6c15c4_145534.jpg', 0, '2020-02-06 14:58:07', '2020-02-06 14:58:07', 0);
INSERT INTO `jcz_product_picture` VALUES (9, 7, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/a2236af3-c89e-434b-a1b8-faf887fbed7a_145752.jpg', 1, '2020-02-06 14:58:08', '2020-02-06 14:58:08', 0);
INSERT INTO `jcz_product_picture` VALUES (10, 7, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/9d8e51ad-3525-47b3-b1dd-74f907fee5e3_145756.jpg', 1, '2020-02-06 14:58:08', '2020-02-06 14:58:08', 0);
INSERT INTO `jcz_product_picture` VALUES (11, 7, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/aef5fb9e-c03f-48a9-b3dd-45003936a4ca_145804.jpg', 1, '2020-02-06 14:58:08', '2020-02-06 14:58:08', 0);
INSERT INTO `jcz_product_picture` VALUES (12, 8, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/50793c76-99d1-42e8-a5bb-25d96197b3e1_145846.jpg', 0, '2020-02-06 14:59:02', '2020-02-06 14:59:02', 0);
INSERT INTO `jcz_product_picture` VALUES (13, 8, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/086b6519-05a1-4e4d-9bd4-41f760885702_145849.jpg', 1, '2020-02-06 14:59:02', '2020-02-06 14:59:02', 0);
INSERT INTO `jcz_product_picture` VALUES (14, 9, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/50793c76-99d1-42e8-a5bb-25d96197b3e1_145846.jpg', 0, '2020-02-06 14:59:20', '2020-02-06 14:59:20', 0);
INSERT INTO `jcz_product_picture` VALUES (15, 9, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/086b6519-05a1-4e4d-9bd4-41f760885702_145849.jpg', 1, '2020-02-06 14:59:20', '2020-02-06 14:59:20', 0);
INSERT INTO `jcz_product_picture` VALUES (16, 9, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/89a1ad60-c3e3-4c6c-b287-0da5363ac7c0_145917.jpg', 1, '2020-02-06 14:59:20', '2020-02-06 14:59:20', 0);
INSERT INTO `jcz_product_picture` VALUES (17, 10, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/72b785ad-1181-4dc6-ac62-3c8a8d1fcd33_145933.jpg', 0, '2020-02-06 14:59:43', '2020-02-06 14:59:43', 0);
INSERT INTO `jcz_product_picture` VALUES (18, 10, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/e3de5fc3-72f3-4b48-a3ee-be277b5279b3_145937.jpg', 1, '2020-02-06 14:59:43', '2020-02-06 14:59:43', 0);
INSERT INTO `jcz_product_picture` VALUES (19, 11, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/073ab1f5-01e1-48d7-bd90-6082a8be470d_151349.jpg', 0, '2020-02-06 15:14:09', '2020-02-06 15:14:09', 0);
INSERT INTO `jcz_product_picture` VALUES (20, 11, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/4c70527a-8c2b-47bc-a9dc-8491edf23353_151354.jpg', 1, '2020-02-06 15:14:09', '2020-02-06 15:14:09', 0);
INSERT INTO `jcz_product_picture` VALUES (21, 11, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/26439bee-f161-4a03-a730-cbdd648ea0f4_151400.jpg', 1, '2020-02-06 15:14:09', '2020-02-06 15:14:09', 0);
INSERT INTO `jcz_product_picture` VALUES (22, 12, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/073ab1f5-01e1-48d7-bd90-6082a8be470d_151349.jpg', 0, '2020-02-06 15:14:18', '2020-02-06 15:14:18', 0);
INSERT INTO `jcz_product_picture` VALUES (23, 12, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/4c70527a-8c2b-47bc-a9dc-8491edf23353_151354.jpg', 1, '2020-02-06 15:14:18', '2020-02-06 15:14:18', 0);
INSERT INTO `jcz_product_picture` VALUES (24, 12, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/26439bee-f161-4a03-a730-cbdd648ea0f4_151400.jpg', 1, '2020-02-06 15:14:18', '2020-02-06 15:14:18', 0);
INSERT INTO `jcz_product_picture` VALUES (25, 12, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/47cc96b8-01c0-4b59-b55b-984fe4beefe9_151413.jpg', 1, '2020-02-06 15:14:18', '2020-02-06 15:14:18', 0);
INSERT INTO `jcz_product_picture` VALUES (46, 17, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/2902bf17-2e1d-4a47-8371-fb4e4e15c55c_095037.jpg', 0, '2020-02-11 09:50:45', '2020-02-11 09:50:45', 0);
INSERT INTO `jcz_product_picture` VALUES (47, 18, 'fdfd23221,fd232fd12,323fdd6,7323wew7', 0, '2020-02-11 10:37:04', '2020-02-11 10:37:04', 0);
INSERT INTO `jcz_product_picture` VALUES (48, 19, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/3c6348f9-d23d-4e8e-b9f6-38793259facc_104411.jpg', 0, '2020-02-11 10:44:31', '2020-02-11 10:44:31', 0);
INSERT INTO `jcz_product_picture` VALUES (49, 19, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/8335c46d-6256-42da-a8bc-8d33236827a5_104415.jpg', 1, '2020-02-11 10:44:31', '2020-02-11 10:44:31', 0);
INSERT INTO `jcz_product_picture` VALUES (50, 20, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/3c6348f9-d23d-4e8e-b9f6-38793259facc_104411.jpg', 0, '2020-02-11 10:48:54', '2020-02-11 10:48:54', 0);
INSERT INTO `jcz_product_picture` VALUES (51, 20, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/8335c46d-6256-42da-a8bc-8d33236827a5_104415.jpg', 1, '2020-02-11 10:48:54', '2020-02-11 10:48:54', 0);
INSERT INTO `jcz_product_picture` VALUES (52, 21, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/3c6348f9-d23d-4e8e-b9f6-38793259facc_104411.jpg', 0, '2020-02-11 11:00:43', '2020-02-11 11:00:43', 0);
INSERT INTO `jcz_product_picture` VALUES (53, 21, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/8335c46d-6256-42da-a8bc-8d33236827a5_104415.jpg', 1, '2020-02-11 11:00:43', '2020-02-11 11:00:43', 0);
INSERT INTO `jcz_product_picture` VALUES (54, 22, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/3c6348f9-d23d-4e8e-b9f6-38793259facc_104411.jpg', 0, '2020-02-11 11:06:10', '2020-02-11 11:06:10', 0);
INSERT INTO `jcz_product_picture` VALUES (55, 22, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/8335c46d-6256-42da-a8bc-8d33236827a5_104415.jpg', 1, '2020-02-11 11:06:10', '2020-02-11 11:06:10', 0);
INSERT INTO `jcz_product_picture` VALUES (56, 16, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/20525c0f-a2d4-4e14-beeb-62ad03177167_141237.jpg', 0, '2020-02-11 14:13:12', '2020-02-11 14:13:12', 0);
INSERT INTO `jcz_product_picture` VALUES (57, 16, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/0fbf7c96-4c2c-43d6-a0fe-8b16b8cdd839_141245.jpg', 1, '2020-02-11 14:13:12', '2020-02-11 14:13:12', 0);
INSERT INTO `jcz_product_picture` VALUES (58, 16, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/6bc3216d-4bdf-44fe-976e-2fd9e408910d_141257.jpg', 1, '2020-02-11 14:13:12', '2020-02-11 14:13:12', 0);
INSERT INTO `jcz_product_picture` VALUES (59, 15, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/c8783464-585a-4f57-8fd1-254ad092c617_141514.jpg', 0, '2020-02-11 14:15:48', '2020-02-11 14:15:48', 0);
INSERT INTO `jcz_product_picture` VALUES (60, 15, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/750d5307-939b-4c6f-b702-48cc81a85962_141528.jpg', 1, '2020-02-11 14:15:48', '2020-02-11 14:15:48', 0);
INSERT INTO `jcz_product_picture` VALUES (61, 15, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/96c66e73-1998-480f-85cc-b98468043851_141534.jpg', 1, '2020-02-11 14:15:48', '2020-02-11 14:15:48', 0);
INSERT INTO `jcz_product_picture` VALUES (62, 23, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/c689a301-5107-4957-9c96-1ad578020bde_144203.jpg', 0, '2020-02-11 14:42:11', '2020-02-11 14:42:11', 0);
INSERT INTO `jcz_product_picture` VALUES (64, 25, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/8604d70f-6d5d-4cc4-9f3f-1cbb1cc07e24_150330.jpg', 0, '2020-02-11 15:04:32', '2020-02-11 15:04:32', 0);
INSERT INTO `jcz_product_picture` VALUES (65, 25, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/9ed3ef31-1150-4f4e-a0f4-03d7ab0deb43_150338.jpg', 1, '2020-02-11 15:04:32', '2020-02-11 15:04:32', 0);
INSERT INTO `jcz_product_picture` VALUES (66, 25, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/09e83f06-620b-476d-8fe4-a65d7d67cdc8_150345.jpg', 1, '2020-02-11 15:04:32', '2020-02-11 15:04:32', 0);
INSERT INTO `jcz_product_picture` VALUES (67, 25, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/7bebb68e-33de-43dc-a0b0-eee763ed1202_150358.jpg', 1, '2020-02-11 15:04:32', '2020-02-11 15:04:32', 0);
INSERT INTO `jcz_product_picture` VALUES (69, 24, '{\"uid\":1581413768960,\"url\":\"24\",\"status\":\"success\"}', 0, '2020-02-11 17:36:28', '2020-02-11 17:36:28', 0);
INSERT INTO `jcz_product_picture` VALUES (74, 29, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/12/3f4a2a4f-d5b8-4ce4-92df-146604520828_174040.jpg', 0, '2020-02-12 17:40:53', '2020-02-12 17:40:53', 0);
INSERT INTO `jcz_product_picture` VALUES (75, 29, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/12/0837ab6f-6661-4e40-a08d-fa4895226262_174044.jpg', 1, '2020-02-12 17:40:53', '2020-02-12 17:40:53', 0);
INSERT INTO `jcz_product_picture` VALUES (76, 29, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/12/29046dc6-4f38-440d-bdec-dad785bb82c3_174047.jpg', 1, '2020-02-12 17:40:53', '2020-02-12 17:40:53', 0);
INSERT INTO `jcz_product_picture` VALUES (81, 30, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/12/bbf8fb77-08c4-4b18-8e25-e3531d0bd347_180039.png', 0, '2020-02-12 18:00:43', '2020-02-12 18:00:43', 0);
INSERT INTO `jcz_product_picture` VALUES (82, 31, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/3c6348f9-d23d-4e8e-b9f6-38793259facc_104411.jpg', 0, '2020-02-12 18:04:34', '2020-02-12 18:04:34', 0);
INSERT INTO `jcz_product_picture` VALUES (83, 31, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/11/8335c46d-6256-42da-a8bc-8d33236827a5_104415.jpg', 1, '2020-02-12 18:04:34', '2020-02-12 18:04:34', 0);
INSERT INTO `jcz_product_picture` VALUES (86, 14, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/09/e44a98fd-b556-4fb5-b343-9adc075ca6e3_150424.jpg', 0, '2020-03-09 15:04:44', '2020-03-09 15:04:44', 0);
INSERT INTO `jcz_product_picture` VALUES (92, 28, '{\"uid\":1583912030035,\"url\":\"{\\\"uid\\\":1581500974284,\\\"url\\\":\\\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/12/bf88275f-47b4-43e7-b818-e0a688736432_173411.jpg\\\",\\\"status\\\":\\\"success\\\"}\",\"status\":\"success\"}', 0, '2020-03-11 15:33:52', '2020-03-11 15:33:52', 0);
INSERT INTO `jcz_product_picture` VALUES (93, 28, '{\"uid\":1583912030036,\"url\":\"{\\\"uid\\\":1581500974285,\\\"url\\\":\\\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/12/52fa5043-59b4-498a-ada3-eaf3534a6ca5_173415.jpg\\\",\\\"status\\\":\\\"success\\\"}\",\"status\":\"success\"}', 1, '2020-03-11 15:33:52', '2020-03-11 15:33:52', 0);
INSERT INTO `jcz_product_picture` VALUES (94, 28, '{\"uid\":1583912030037,\"url\":\"{\\\"uid\\\":1581500974286,\\\"url\\\":\\\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/12/53ff3f5d-5813-496f-808e-e492e9753842_173418.jpg\\\",\\\"status\\\":\\\"success\\\"}\",\"status\":\"success\"}', 1, '2020-03-11 15:33:52', '2020-03-11 15:33:52', 0);
INSERT INTO `jcz_product_picture` VALUES (95, 27, '{\"uid\":1583912035808,\"url\":\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/12/c62d39ee-dc83-4bbc-93fc-c940bbfdc9ee_165401.jpg\",\"status\":\"success\"}', 0, '2020-03-11 15:33:59', '2020-03-11 15:33:59', 0);
INSERT INTO `jcz_product_picture` VALUES (102, 1, '{\"uid\":1583921307413,\"url\":\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/11c54fa4-b141-4a57-bc65-80d5da5def79_233646.jpg\",\"status\":\"success\"}', 0, '2020-03-11 18:09:11', '2020-03-11 18:09:11', 0);
INSERT INTO `jcz_product_picture` VALUES (103, 13, '{\"uid\":1583921514408,\"url\":\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/fb71bf51-54a0-4f29-b0e7-02549ebe10b4_152659.jpg\",\"status\":\"success\"}', 0, '2020-03-11 18:12:03', '2020-03-11 18:12:03', 0);
INSERT INTO `jcz_product_picture` VALUES (104, 13, '{\"uid\":1583921514409,\"url\":\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/3ed0dd09-9721-4e40-816e-035b90c55cd4_152700.jpg\",\"status\":\"success\"}', 1, '2020-03-11 18:12:03', '2020-03-11 18:12:03', 0);
INSERT INTO `jcz_product_picture` VALUES (105, 13, '{\"uid\":1583921514410,\"url\":\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/fce0da94-9f77-4413-80cf-55f15ea5c335_152706.jpg\",\"status\":\"success\"}', 1, '2020-03-11 18:12:03', '2020-03-11 18:12:03', 0);
INSERT INTO `jcz_product_picture` VALUES (106, 13, '{\"uid\":1583921514411,\"url\":\"http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/02/06/7edf244b-9992-454b-b667-a2f10fdeb0ca_154007.jpg\",\"status\":\"success\"}', 1, '2020-03-11 18:12:03', '2020-03-11 18:12:03', 0);
INSERT INTO `jcz_product_picture` VALUES (108, 32, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/dbc95f53-3cd9-4241-84df-ff69cce11da7_182807.jpg', 0, '2020-03-11 18:28:12', '2020-03-11 18:28:12', 0);
INSERT INTO `jcz_product_picture` VALUES (126, 33, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/11/d2bf5bd3-acdd-4a12-ac2d-5b4f539a983d_190846.jpg', 0, '2020-03-11 19:16:51', '2020-03-11 19:16:51', 0);
INSERT INTO `jcz_product_picture` VALUES (128, 26, 'http://jk-project.oss-cn-beijing.aliyuncs.com/image/2020/03/12/3ec57fbe-c42f-4353-aa83-fc8a56b918b3_110126.jpg', 0, '2020-03-12 11:01:41', '2020-03-12 11:01:41', 0);

-- ----------------------------
-- Table structure for jcz_sys_admin
-- ----------------------------
DROP TABLE IF EXISTS `jcz_sys_admin`;
CREATE TABLE `jcz_sys_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `authority` int(10) NOT NULL COMMENT '管理员权限 1-管理员  2-用户',
  `user_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '真实姓名',
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '123456' COMMENT '密码',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 否 1 是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jcz_sys_admin
-- ----------------------------
INSERT INTO `jcz_sys_admin` VALUES (1, 2, '123456', '18234509474', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-05 11:08:21', '2020-02-05 11:08:21', 0);
INSERT INTO `jcz_sys_admin` VALUES (2, 2, '123', '18234509474', '123', '2020-02-05 11:44:15', '2020-02-05 11:44:15', 0);
INSERT INTO `jcz_sys_admin` VALUES (3, 2, '123456', '13121558494', 'fcea920f7412b5da7be0cf42b8c93759', '2020-02-05 20:08:40', '2020-02-05 20:08:40', 0);
INSERT INTO `jcz_sys_admin` VALUES (4, 1, '12345', '12345', '12345', '2020-02-06 11:53:21', '2020-02-06 11:53:21', 0);
INSERT INTO `jcz_sys_admin` VALUES (5, 2, '123', '18822178013', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-07 20:04:10', '2020-02-07 20:04:10', 0);
INSERT INTO `jcz_sys_admin` VALUES (6, 2, '李丽丽', '18202261410', '9efebb3d7d059bff092842bf31dd2816', '2020-02-10 17:47:30', '2020-02-10 17:47:30', 0);
INSERT INTO `jcz_sys_admin` VALUES (7, 2, '李丽', '18202261411', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-10 18:00:10', '2020-02-10 18:00:10', 0);
INSERT INTO `jcz_sys_admin` VALUES (8, 2, '测试', '18202261412', '628631f07321b22d8c176c200c855e1b', '2020-02-10 18:27:12', '2020-02-10 18:27:12', 0);
INSERT INTO `jcz_sys_admin` VALUES (9, 2, '用户名1', '18202261413', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-11 09:11:27', '2020-02-11 09:11:27', 0);
INSERT INTO `jcz_sys_admin` VALUES (10, 2, 'fujianhua', '17822015180', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-11 10:03:57', '2020-02-11 10:03:57', 0);
INSERT INTO `jcz_sys_admin` VALUES (11, 2, 'wang', '18835513898', '723d505516e0c197e42a6be3c0af910e', '2020-02-11 15:01:50', '2020-02-11 15:01:50', 0);
INSERT INTO `jcz_sys_admin` VALUES (12, 2, '111111', '18234509475', 'a127fd1f86e4ab650f2216f09992afa4', '2020-02-11 15:40:15', '2020-02-11 15:40:15', 0);
INSERT INTO `jcz_sys_admin` VALUES (13, 2, '用户名测试1', '18202261422', 'a127fd1f86e4ab650f2216f09992afa4', '2020-02-11 17:29:34', '2020-02-11 17:29:34', 0);
INSERT INTO `jcz_sys_admin` VALUES (14, 2, '用户名测试2', '13212341234', 'f9bd0091fd722d3925cfe30eae241e04', '2020-02-11 17:30:40', '2020-02-11 17:30:40', 0);
INSERT INTO `jcz_sys_admin` VALUES (15, 2, '李丽丽', '18202262222', 'fcea920f7412b5da7be0cf42b8c93759', '2020-02-12 09:32:19', '2020-02-12 09:32:19', 0);
INSERT INTO `jcz_sys_admin` VALUES (16, 2, '```````', '15512341234', 'fcea920f7412b5da7be0cf42b8c93759', '2020-02-12 10:32:04', '2020-02-12 10:32:04', 0);
INSERT INTO `jcz_sys_admin` VALUES (17, 2, '====', '15512341233', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 10:33:02', '2020-02-12 10:33:02', 0);
INSERT INTO `jcz_sys_admin` VALUES (18, 2, '123', '13312341234', '343b1c4a3ea721b2d640fc8700db0f36', '2020-02-12 10:36:15', '2020-02-12 10:36:15', 0);
INSERT INTO `jcz_sys_admin` VALUES (19, 2, 'try123', '13312341233', '46f94c8de14fb36680850768ff1b7f2a', '2020-02-12 11:03:28', '2020-02-12 11:03:28', 0);
INSERT INTO `jcz_sys_admin` VALUES (20, 2, '123456789', '18835513889', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 11:05:48', '2020-02-12 11:05:48', 0);
INSERT INTO `jcz_sys_admin` VALUES (21, 2, 'try111', '14312341234', '46f94c8de14fb36680850768ff1b7f2a', '2020-02-12 11:06:22', '2020-02-12 11:06:22', 0);
INSERT INTO `jcz_sys_admin` VALUES (22, 2, 'wangxin', '13222222222', 'fa45c1512950c2c1b7d34f3deb6b6006', '2020-02-12 11:15:21', '2020-02-12 11:15:21', 0);
INSERT INTO `jcz_sys_admin` VALUES (23, 2, 'wang2xin', '13222212222', 'fa45c1512950c2c1b7d34f3deb6b6006', '2020-02-12 11:18:56', '2020-02-12 11:18:56', 0);
INSERT INTO `jcz_sys_admin` VALUES (24, 2, 'wangxin...', '13222222223', '25f9e794323b453885f5181f1b624d0b', '2020-02-12 11:21:04', '2020-02-12 11:21:04', 0);
INSERT INTO `jcz_sys_admin` VALUES (25, 2, 'asd@#1', '13121558498', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 12:13:57', '2020-02-12 12:13:57', 0);
INSERT INTO `jcz_sys_admin` VALUES (26, 2, '123456789', '13555555555', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 12:14:45', '2020-02-12 12:14:45', 0);
INSERT INTO `jcz_sys_admin` VALUES (27, 2, '123...', '13233333333', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 14:02:13', '2020-02-12 14:02:13', 0);
INSERT INTO `jcz_sys_admin` VALUES (28, 2, '```', '14412341234', '4297f44b13955235245b2497399d7a93', '2020-02-12 14:09:34', '2020-02-12 14:09:34', 0);
INSERT INTO `jcz_sys_admin` VALUES (29, 2, 'try12···,', '14412341233', '4297f44b13955235245b2497399d7a93', '2020-02-12 14:10:18', '2020-02-12 14:10:18', 0);
INSERT INTO `jcz_sys_admin` VALUES (30, 2, '@#$%^$%%', '13121441231', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 14:10:50', '2020-02-12 14:10:50', 0);
INSERT INTO `jcz_sys_admin` VALUES (31, 2, 'tyr123....', '13123451234', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 14:14:10', '2020-02-12 14:14:10', 0);
INSERT INTO `jcz_sys_admin` VALUES (32, 2, '·····', '13211231231', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 14:16:01', '2020-02-12 14:16:01', 0);
INSERT INTO `jcz_sys_admin` VALUES (33, 2, 'trytest', '15512341232', 'd02ebde979264c79e141559fc9f6b65c', '2020-02-12 14:16:40', '2020-02-12 14:16:40', 0);
INSERT INTO `jcz_sys_admin` VALUES (34, 2, '123....', '13244444444', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 14:17:10', '2020-02-12 14:17:10', 0);
INSERT INTO `jcz_sys_admin` VALUES (35, 2, 'ccc', '13331231231', '4297f44b13955235245b2497399d7a93', '2020-02-12 14:19:32', '2020-02-12 14:19:32', 0);
INSERT INTO `jcz_sys_admin` VALUES (36, 2, 'tyr123···222222222222', '13255555555', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-12 14:30:16', '2020-02-12 14:30:16', 0);
INSERT INTO `jcz_sys_admin` VALUES (37, 2, '测试', '18281701653', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-09 14:21:02', '2020-03-09 14:21:02', 0);
INSERT INTO `jcz_sys_admin` VALUES (38, 2, '18875767707', '18875767707', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-20 18:00:53', '2020-03-20 18:00:53', 0);

-- ----------------------------
-- Table structure for jcz_user
-- ----------------------------
DROP TABLE IF EXISTS `jcz_user`;
CREATE TABLE `jcz_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '真实姓名',
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号',
  `contact_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人姓名',
  `contact_phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人电话',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 否 1 是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jcz_user
-- ----------------------------
INSERT INTO `jcz_user` VALUES (1, '123456', '', '456789', '18234509474', '123@qq.com', '2020-02-05 11:07:22', '2020-02-05 11:07:22', 0);
INSERT INTO `jcz_user` VALUES (3, '123', NULL, '456', '18234509474', '456@qq.com', '2020-02-05 11:44:15', '2020-02-05 11:44:15', 0);
INSERT INTO `jcz_user` VALUES (4, '123456', NULL, '葛志成', '13121558494', NULL, '2020-02-05 20:08:40', '2020-02-05 20:08:40', 0);
INSERT INTO `jcz_user` VALUES (5, '12345', '12345', 'asd', '12345', '123', '2020-02-06 11:53:43', '2020-02-06 11:53:43', 0);
INSERT INTO `jcz_user` VALUES (6, '123', NULL, '123', '18822178013', NULL, '2020-02-07 20:04:10', '2020-02-07 20:04:10', 0);
INSERT INTO `jcz_user` VALUES (7, '李丽丽', NULL, '123', '18202261410', NULL, '2020-02-10 17:47:30', '2020-02-10 17:47:30', 0);
INSERT INTO `jcz_user` VALUES (8, '李丽', NULL, '玲玲', '18202261411', NULL, '2020-02-10 18:00:10', '2020-02-10 18:00:10', 0);
INSERT INTO `jcz_user` VALUES (9, '测试', NULL, '测试', '18202261412', NULL, '2020-02-10 18:27:12', '2020-02-10 18:27:12', 0);
INSERT INTO `jcz_user` VALUES (10, '用户名1', NULL, '周岁', '18202261413', NULL, '2020-02-11 09:11:27', '2020-02-11 09:11:27', 0);
INSERT INTO `jcz_user` VALUES (11, 'fujianhua', NULL, 'll', '17822015180', NULL, '2020-02-11 10:03:57', '2020-02-11 10:03:57', 0);
INSERT INTO `jcz_user` VALUES (12, 'wang', NULL, 'wang', '18835513898', NULL, '2020-02-11 15:01:50', '2020-02-11 15:01:50', 0);
INSERT INTO `jcz_user` VALUES (13, '111111', NULL, '1', '18234509475', NULL, '2020-02-11 15:40:15', '2020-02-11 15:40:15', 0);
INSERT INTO `jcz_user` VALUES (14, '用户名测试1', NULL, '空格', '18202261422', NULL, '2020-02-11 17:29:34', '2020-02-11 17:29:34', 0);
INSERT INTO `jcz_user` VALUES (15, '用户名测试2', NULL, '测试', '13212341234', NULL, '2020-02-11 17:30:40', '2020-02-11 17:30:40', 0);
INSERT INTO `jcz_user` VALUES (16, '李丽丽', NULL, '用户名重复', '18202262222', NULL, '2020-02-12 09:32:19', '2020-02-12 09:32:19', 0);
INSERT INTO `jcz_user` VALUES (17, '```````', NULL, 'cs', '15512341234', NULL, '2020-02-12 10:32:04', '2020-02-12 10:32:04', 0);
INSERT INTO `jcz_user` VALUES (18, '====', NULL, 'cs', '15512341233', NULL, '2020-02-12 10:33:02', '2020-02-12 10:33:02', 0);
INSERT INTO `jcz_user` VALUES (19, '123', NULL, 'lili.', '13312341234', NULL, '2020-02-12 10:36:15', '2020-02-12 10:36:15', 0);
INSERT INTO `jcz_user` VALUES (20, 'try123', NULL, 'asd', '13312341233', NULL, '2020-02-12 11:03:28', '2020-02-12 11:03:28', 0);
INSERT INTO `jcz_user` VALUES (21, '123456789', NULL, 'qwe', '18835513889', NULL, '2020-02-12 11:05:48', '2020-02-12 11:05:48', 0);
INSERT INTO `jcz_user` VALUES (22, 'try111', NULL, 'asd12`', '14312341234', NULL, '2020-02-12 11:06:22', '2020-02-12 11:06:22', 0);
INSERT INTO `jcz_user` VALUES (23, 'wangxin', NULL, '123456', '13222222222', '456@qq.com', '2020-02-12 11:15:21', '2020-02-12 11:15:21', 0);
INSERT INTO `jcz_user` VALUES (24, 'wang2xin', NULL, '123456', '13222212222', '456@qq.com', '2020-02-12 11:18:56', '2020-02-12 11:18:56', 0);
INSERT INTO `jcz_user` VALUES (25, 'wangxin...', NULL, '123456', '13222222223', '456@qq.com', '2020-02-12 11:21:04', '2020-02-12 11:21:04', 0);
INSERT INTO `jcz_user` VALUES (26, 'asd@#1', NULL, '格致诚', '13121558498', NULL, '2020-02-12 12:13:57', '2020-02-12 12:13:57', 0);
INSERT INTO `jcz_user` VALUES (27, '123456789', NULL, '123456', '13555555555', NULL, '2020-02-12 12:14:45', '2020-02-12 12:14:45', 0);
INSERT INTO `jcz_user` VALUES (28, '123...', NULL, 'qwe', '13233333333', NULL, '2020-02-12 14:02:13', '2020-02-12 14:02:13', 0);
INSERT INTO `jcz_user` VALUES (29, '```', NULL, '测试', '14412341234', NULL, '2020-02-12 14:09:34', '2020-02-12 14:09:34', 0);
INSERT INTO `jcz_user` VALUES (30, 'try12···,', NULL, '测试', '14412341233', NULL, '2020-02-12 14:10:18', '2020-02-12 14:10:18', 0);
INSERT INTO `jcz_user` VALUES (31, '@#$%^$%%', NULL, '1234', '13121441231', NULL, '2020-02-12 14:10:50', '2020-02-12 14:10:50', 0);
INSERT INTO `jcz_user` VALUES (32, 'tyr123....', NULL, 'asd', '13123451234', NULL, '2020-02-12 14:14:10', '2020-02-12 14:14:10', 0);
INSERT INTO `jcz_user` VALUES (33, '·····', NULL, 'asd', '13211231231', NULL, '2020-02-12 14:16:01', '2020-02-12 14:16:01', 0);
INSERT INTO `jcz_user` VALUES (34, 'trytest', NULL, 'mimi', '15512341232', NULL, '2020-02-12 14:16:40', '2020-02-12 14:16:40', 0);
INSERT INTO `jcz_user` VALUES (35, '123....', NULL, '123', '13244444444', NULL, '2020-02-12 14:17:10', '2020-02-12 14:17:10', 0);
INSERT INTO `jcz_user` VALUES (36, 'ccc', NULL, 'yr````···', '13331231231', NULL, '2020-02-12 14:19:32', '2020-02-12 14:19:32', 0);
INSERT INTO `jcz_user` VALUES (37, 'tyr123···222222222222', NULL, '123456789..', '13255555555', NULL, '2020-02-12 14:30:16', '2020-02-12 14:30:16', 0);
INSERT INTO `jcz_user` VALUES (38, '测试', NULL, '呵', '18281701653', NULL, '2020-03-09 14:21:02', '2020-03-09 14:21:02', 0);
INSERT INTO `jcz_user` VALUES (39, '18875767707', NULL, '赵树超', '18875767707', NULL, '2020-03-20 18:00:53', '2020-03-20 18:00:53', 0);

SET FOREIGN_KEY_CHECKS = 1;
