package com.yuanjun.product.config;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class CCommonSettings {
    protected static ResourceBundle source = PropertyResourceBundle.getBundle("config/c_common");

    public static int getIntProperty(String key) {
        return Integer.valueOf(source.getString(key)).intValue();
    }

    public static boolean getBooleanProperty(String key) {
        return Boolean.valueOf(source.getString(key)).booleanValue();
    }

    public static double getDoubleProperty(String key) {
        return Double.valueOf(source.getString(key)).doubleValue();
    }

    public static String getProperty(String key) {
        return source.getString(key);
    }
}
