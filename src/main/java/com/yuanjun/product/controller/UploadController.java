package com.yuanjun.product.controller;


import com.yuanjun.product.service.ImageService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author zsc
 * @date 2020/2/4
 */
@Slf4j
@RestController
@RequestMapping("/uploadFile")
public class UploadController {
    @Autowired
    private ImageService imageService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @PostMapping(value = "/uploadImage")
    public BaseResult uploadImage(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestParam("file") MultipartFile file, @RequestParam("imgType") byte imgType) {
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        //type 1 非加密的 2 加密
        String url = imageService.uploadImage(file, imgType);
        if (url == null) {
            return BaseResult.createFail(Const.HttpRespCode.BAD_REQUEST, "上传失败");
        }
        log.info("url:{},",url);
        return BaseResult.createOk(url);
    }

}
