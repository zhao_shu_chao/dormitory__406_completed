package com.yuanjun.product.controller.admin;

import com.yuanjun.product.service.AdminIndexService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: ZSC
 * @time: 2020/2/5
 */

@Slf4j
@RestController
@RequestMapping("/admin")
public class AdminIndexController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AdminIndexService adminIndexService ;
    @RequestMapping("/index")
    public BaseResult adminIndex(@RequestHeader(Const.PROADMINTOKEN) String token){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        log.info("token:{}",token);
        return BaseResult.createOk(adminIndexService.adminIndex()) ;
    }
    /**
     * 登录注册
     * 轮播
     * 商品展示
     * 商品详情
     * 订单预览
     * 收货地址
     * 提交订单
     * 我的进货单（自己的订单）
     *
     */

    /**
     登录
     首页
     添加商品
     商品管理
     banner管理
     订单管理
     用户管理
     去发货
     */

}
