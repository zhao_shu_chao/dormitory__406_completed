package com.yuanjun.product.controller.admin;

import com.alibaba.fastjson.JSON;
import com.yuanjun.product.service.BannerService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.ReqBannerVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author ZSC
 * @date 2020/2/4
 * banner图管理
 */
@Slf4j
@RestController
@RequestMapping("/banner")
public class BannerController {
    @Resource
    private BannerService bannerService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @RequestMapping("/selectBannerList")
    public BaseResult selectBannerList(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestBody ReqBannerVo reqBannerVo){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        log.info("param:{}", JSON.toJSONString(reqBannerVo));
        return BaseResult.createOk(bannerService.selectBannerList(reqBannerVo));
    }

    @PostMapping("/addBanner")
    public BaseResult addBanner(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestBody ReqBannerVo reqBannerVo){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        BaseResult result = checkBanner(reqBannerVo);
        if (!result.isOk()) {
            return result;
        }
        return bannerService.addBanner(reqBannerVo);
    }
    @GetMapping("/selectBannerById")
    public BaseResult selectById(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestParam Integer id){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        return bannerService.selectById(id);
    }

    @PostMapping("/updateBanner")
    public BaseResult updateBanner(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestBody ReqBannerVo reqBannerVo){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        BaseResult result = checkBanner(reqBannerVo);
        if (!result.isOk()) {
            return result;
        }
        return bannerService.updateBanner(reqBannerVo);
    }

    @GetMapping("/deleteBanner")
    public BaseResult deleteBanner(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestParam Integer id){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        return bannerService.deleteBanner(id);
    }

    private BaseResult checkBanner(ReqBannerVo banner) {
        if (StringUtils.isEmpty(banner.getBannerName())) {
            return BaseResult.createFail(Const.HttpRespCode.INCOMPATIBLE_CONDITION, "名称不能为空");
        }
        if (StringUtils.isEmpty(banner.getBannerJumpUrl())) {
            return BaseResult.createFail(Const.HttpRespCode.INCOMPATIBLE_CONDITION, "跳转链接不能为空");
        }
        if (banner.getLocation() == null) {
            return BaseResult.createFail(Const.HttpRespCode.INCOMPATIBLE_CONDITION, "位置不能为空");
        }
        return BaseResult.createOk("");
    }
}
