package com.yuanjun.product.controller.admin;

import com.alibaba.fastjson.JSON;
import com.yuanjun.product.service.OrderManageService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.ReqOrderManageVo;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * @author: ZSC
 * @time: 2020/2/4
 * @title:后台订单管理
 */

@Slf4j
@RestController
@RequestMapping(value = "/admin")
public class OrderManagementController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private OrderManageService orderManageService ;

    /**
     * 订单管理
     * @param reqOrderManageVo
     * @return
     */
    @RequestMapping("/orderManage")
    public BaseResult orderManage(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestBody ReqOrderManageVo reqOrderManageVo){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        log.info("token:{},param:{}",token, JSON.toJSONString(reqOrderManageVo));
        return BaseResult.createOk(orderManageService.orderManage(reqOrderManageVo));
    }

    /**
     * 发货
     * @param orderId
     * @return
     */
    @RequestMapping("/sendProduct")
    public BaseResult sendProduct(@RequestHeader(Const.PROADMINTOKEN) String token,@RequestParam Integer orderId){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        log.info("token:{},param:{}",token,orderId);
        return orderManageService.sendProduct(orderId);
    }
}
