package com.yuanjun.product.controller.admin;

import com.yuanjun.product.service.ProductManageService;
import com.yuanjun.product.service.ReleaseProductService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.ReqAddProductVo;
import com.yuanjun.product.vo.resp.RespUpdateProductLoadDateVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/product")
public class ProductManageController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private ProductManageService productManageService;
    @Resource
    private ReleaseProductService releaseProductService;

    /**
     * 商品下架
     * @param id
     * @return
     */
    @RequestMapping("/underCarriage")
    public BaseResult underCarriage(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestParam Integer id){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        return productManageService.underCarriage(id);
    }

    @RequestMapping("/shelves")
    public BaseResult shelvesProduct(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestParam Integer id){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        return productManageService.shelvesProduct(id);
    }


    /**
     * 加载商品信息
     * @param id
     * @return
     */
    @RequestMapping("/getProductById")
    public BaseResult loadProduct(@RequestHeader(Const.PROADMINTOKEN) String token,@RequestParam Integer id){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        return productManageService.loadProduct(id);
    }

    /**
     * 修改商品信息
     * @param token
     * @param respUpdateProductLoadDateVo
     * @return
     */
    @RequestMapping("/updateProduct")
    public BaseResult updateProduct(@RequestHeader(Const.PROADMINTOKEN) String token,@RequestBody RespUpdateProductLoadDateVo respUpdateProductLoadDateVo){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        return productManageService.updateProductById(respUpdateProductLoadDateVo);
    }
}
