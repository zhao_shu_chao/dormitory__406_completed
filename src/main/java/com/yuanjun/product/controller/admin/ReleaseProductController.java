package com.yuanjun.product.controller.admin;

import com.alibaba.fastjson.JSON;
import com.yuanjun.product.service.ReleaseProductService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.ReqAddProductVo;
import com.yuanjun.product.vo.req.ReqAdminProductListVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: ZSC
 * @time: 2020/2/5
 * 商品管理
 */

@Slf4j
@RestController
@RequestMapping(value = "/admin")
public class ReleaseProductController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ReleaseProductService productManageService ;

    /**
     * 发布商品
     * @param token
     * @param reqAddProductVo
     * @return
     */
    @RequestMapping("/releaseProduct")
    public BaseResult releaseProduct(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestBody ReqAddProductVo reqAddProductVo){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        return productManageService.releaseProduct(reqAddProductVo);
    }

    /**
     * 后台商品列表
     * @param token
     * @param reqAdminProductListVo
     * @return
     */
    @RequestMapping("/adminProductList")
    public BaseResult AdminProductList(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestBody ReqAdminProductListVo reqAdminProductListVo){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        log.info("token:{},param:{}",token, JSON.toJSONString(reqAdminProductListVo));
        return BaseResult.createOk(productManageService.adminProductList(reqAdminProductListVo)) ;
    }
}
