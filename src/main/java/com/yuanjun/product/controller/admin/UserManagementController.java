package com.yuanjun.product.controller.admin;

import com.alibaba.fastjson.JSON;
import com.yuanjun.product.service.UserManageService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.ReqUserManageVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: ZSC
 * @time: 2020/2/4
 * @title:后台用户管理
 */
@Slf4j
@RestController
@RequestMapping(value = "/admin")
public class UserManagementController {
    @Autowired
    private StringRedisTemplate stringRedisTemplate ;
    @Autowired
    private UserManageService userManageService ;

    /**
     * 用户管理列表
     * @return
     */
    @RequestMapping("/userManage")
    public BaseResult userManage(@RequestHeader(Const.PROADMINTOKEN) String token, @RequestBody ReqUserManageVo reqUserManageVo){
        String jczSysAdmin = stringRedisTemplate.opsForValue().get(token);
        if (StringUtils.isEmpty(jczSysAdmin)) {
            return BaseResult.createFail(Const.HttpRespCode.TOKEN_INVALID, "登陆已超时,请重新登陆");
        }
        log.info("token:{},param:{}",token, JSON.toJSONString(reqUserManageVo));
        return BaseResult.createOk(userManageService.userList(reqUserManageVo));
    }
}
