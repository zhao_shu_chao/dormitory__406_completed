package com.yuanjun.product.controller.front;

import com.alibaba.fastjson.JSON;
import com.yuanjun.product.entity.JczUser;
import com.yuanjun.product.service.ProductService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.*;
import com.yuanjun.product.vo.base.BaseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ProductService productService;

    /**
     * 产品列表
     *
     * @param param
     * @return
     */
    @RequestMapping("/productList")
    public BaseResult productList(@RequestBody ReqProductListVo param) {
        log.info("param:{}", JSON.toJSONString(param));
        return productService.productList(param);
    }

    /**
     * 产品详情
     *
     * @param param
     * @return
     */
    @RequestMapping("/productDetail")
    public BaseResult productDetail(@RequestBody ReqProductDetailVo param) {
        log.info("param:{}", JSON.toJSONString(param));
        return productService.productDetail(param);
    }


    /**
     * 生成订单
     *
     * @param param
     * @return
     */
    @RequestMapping("/makeOrder")
    public BaseResult makeOrder(@RequestHeader(Const.PROFRONTTOKEN) String token, @RequestBody ReqMakeOrderVo param) {
        log.info("token:{},param:{}", token, JSON.toJSONString(param));
        String userJson = stringRedisTemplate.opsForValue().get(token);
        JczUser user = JSON.parseObject(userJson, JczUser.class);
        return productService.makeOrder(user, param);
    }

    /**
     * 订单列表
     *
     * @param param
     * @return
     */
    @RequestMapping("/OrderList")
    public BaseResult OrderList(@RequestBody ReqConfirmOrderVo param) {
        log.info("param:{}", JSON.toJSONString(param));
        return productService.OrderList(param);
    }

    /**
     *订单表存收货地址id
     * @param reqOrderAddress
     * @return
     */
    @RequestMapping("/orderAddress")
    public BaseResult orderAddress(@RequestBody ReqOrderAddress reqOrderAddress) {
        return productService.orderAddress(reqOrderAddress);
    }

    /**
     * 进货单
     * @param token
     * @return
     */
    @RequestMapping("/ownOrderList")
    public BaseResult ownOrderList(@RequestHeader(Const.PROFRONTTOKEN) String token,@RequestBody ReqOwnOrderListVo reqOwnOrderListVo) {
        log.info("token:{},param:{}", token, JSON.toJSONString(reqOwnOrderListVo));
        String userJson = stringRedisTemplate.opsForValue().get(token);
        JczUser user = JSON.parseObject(userJson, JczUser.class);
        return productService.ownOrderList(user,reqOwnOrderListVo);
    }

    /**
     * 完成付款
     *
     * @return
     */
    @PostMapping("/endPayment")
    public BaseResult endPayment(@RequestBody ReqEndPaymentVo reqEndPaymentVo) {
        return productService.endPayment(reqEndPaymentVo);
    }
}
