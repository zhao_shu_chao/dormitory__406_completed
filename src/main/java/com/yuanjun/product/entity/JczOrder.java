package com.yuanjun.product.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table jcz_order
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class JczOrder {
    /**
     * Database Column Remarks:
     *   主键
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   订单编号
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.order_number
     *
     * @mbg.generated
     */
    private String orderNumber;

    /**
     * Database Column Remarks:
     *   用户id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.user_id
     *
     * @mbg.generated
     */
    private Integer userId;

    /**
     * Database Column Remarks:
     *   收货地址id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.address_id
     *
     * @mbg.generated
     */
    private Integer addressId;

    /**
     * Database Column Remarks:
     *   数量
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.total_count
     *
     * @mbg.generated
     */
    private Integer totalCount;

    /**
     * Database Column Remarks:
     *   交易状态 0-订单初始化 1-待支付 2-待发货 3-已发货
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.status
     *
     * @mbg.generated
     */
    private Byte status;

    /**
     * Database Column Remarks:
     *   支付方式(1-支付宝；2-银行卡)
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.pay_type
     *
     * @mbg.generated
     */
    private Byte payType;

    /**
     * Database Column Remarks:
     *   实收款
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.total_income
     *
     * @mbg.generated
     */
    private BigDecimal totalIncome;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.pay_time
     *
     * @mbg.generated
     */
    private Date payTime;

    /**
     * Database Column Remarks:
     *   创建时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * Database Column Remarks:
     *   修改时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.update_time
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * Database Column Remarks:
     *   0 否 1 是
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_order.delete_flag
     *
     * @mbg.generated
     */
    private Byte deleteFlag;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.id
     *
     * @return the value of jcz_order.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.id
     *
     * @param id the value for jcz_order.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.order_number
     *
     * @return the value of jcz_order.order_number
     *
     * @mbg.generated
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.order_number
     *
     * @param orderNumber the value for jcz_order.order_number
     *
     * @mbg.generated
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber == null ? null : orderNumber.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.user_id
     *
     * @return the value of jcz_order.user_id
     *
     * @mbg.generated
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.user_id
     *
     * @param userId the value for jcz_order.user_id
     *
     * @mbg.generated
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.address_id
     *
     * @return the value of jcz_order.address_id
     *
     * @mbg.generated
     */
    public Integer getAddressId() {
        return addressId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.address_id
     *
     * @param addressId the value for jcz_order.address_id
     *
     * @mbg.generated
     */
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.total_count
     *
     * @return the value of jcz_order.total_count
     *
     * @mbg.generated
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.total_count
     *
     * @param totalCount the value for jcz_order.total_count
     *
     * @mbg.generated
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.status
     *
     * @return the value of jcz_order.status
     *
     * @mbg.generated
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.status
     *
     * @param status the value for jcz_order.status
     *
     * @mbg.generated
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.pay_type
     *
     * @return the value of jcz_order.pay_type
     *
     * @mbg.generated
     */
    public Byte getPayType() {
        return payType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.pay_type
     *
     * @param payType the value for jcz_order.pay_type
     *
     * @mbg.generated
     */
    public void setPayType(Byte payType) {
        this.payType = payType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.total_income
     *
     * @return the value of jcz_order.total_income
     *
     * @mbg.generated
     */
    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.total_income
     *
     * @param totalIncome the value for jcz_order.total_income
     *
     * @mbg.generated
     */
    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.pay_time
     *
     * @return the value of jcz_order.pay_time
     *
     * @mbg.generated
     */
    public Date getPayTime() {
        return payTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.pay_time
     *
     * @param payTime the value for jcz_order.pay_time
     *
     * @mbg.generated
     */
    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.create_time
     *
     * @return the value of jcz_order.create_time
     *
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.create_time
     *
     * @param createTime the value for jcz_order.create_time
     *
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.update_time
     *
     * @return the value of jcz_order.update_time
     *
     * @mbg.generated
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.update_time
     *
     * @param updateTime the value for jcz_order.update_time
     *
     * @mbg.generated
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_order.delete_flag
     *
     * @return the value of jcz_order.delete_flag
     *
     * @mbg.generated
     */
    public Byte getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_order.delete_flag
     *
     * @param deleteFlag the value for jcz_order.delete_flag
     *
     * @mbg.generated
     */
    public void setDeleteFlag(Byte deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}