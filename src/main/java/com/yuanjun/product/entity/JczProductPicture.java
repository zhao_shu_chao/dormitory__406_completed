package com.yuanjun.product.entity;

import java.util.Date;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table jcz_product_picture
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class JczProductPicture {
    /**
     * Database Column Remarks:
     *   图片id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_product_picture.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   产品id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_product_picture.product_id
     *
     * @mbg.generated
     */
    private Integer productId;

    /**
     * Database Column Remarks:
     *   图片地址
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_product_picture.picture_url
     *
     * @mbg.generated
     */
    private String pictureUrl;

    /**
     * Database Column Remarks:
     *   订单显示图(0-显示；1-不显示)
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_product_picture.is_show
     *
     * @mbg.generated
     */
    private Byte isShow;

    /**
     * Database Column Remarks:
     *   创建时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_product_picture.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * Database Column Remarks:
     *   修改时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_product_picture.update_time
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * Database Column Remarks:
     *   是否删除: 0 否 1 是
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column jcz_product_picture.delete_flag
     *
     * @mbg.generated
     */
    private Byte deleteFlag;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_product_picture.id
     *
     * @return the value of jcz_product_picture.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_product_picture.id
     *
     * @param id the value for jcz_product_picture.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_product_picture.product_id
     *
     * @return the value of jcz_product_picture.product_id
     *
     * @mbg.generated
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_product_picture.product_id
     *
     * @param productId the value for jcz_product_picture.product_id
     *
     * @mbg.generated
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_product_picture.picture_url
     *
     * @return the value of jcz_product_picture.picture_url
     *
     * @mbg.generated
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_product_picture.picture_url
     *
     * @param pictureUrl the value for jcz_product_picture.picture_url
     *
     * @mbg.generated
     */
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl == null ? null : pictureUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_product_picture.is_show
     *
     * @return the value of jcz_product_picture.is_show
     *
     * @mbg.generated
     */
    public Byte getIsShow() {
        return isShow;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_product_picture.is_show
     *
     * @param isShow the value for jcz_product_picture.is_show
     *
     * @mbg.generated
     */
    public void setIsShow(Byte isShow) {
        this.isShow = isShow;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_product_picture.create_time
     *
     * @return the value of jcz_product_picture.create_time
     *
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_product_picture.create_time
     *
     * @param createTime the value for jcz_product_picture.create_time
     *
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_product_picture.update_time
     *
     * @return the value of jcz_product_picture.update_time
     *
     * @mbg.generated
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_product_picture.update_time
     *
     * @param updateTime the value for jcz_product_picture.update_time
     *
     * @mbg.generated
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column jcz_product_picture.delete_flag
     *
     * @return the value of jcz_product_picture.delete_flag
     *
     * @mbg.generated
     */
    public Byte getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column jcz_product_picture.delete_flag
     *
     * @param deleteFlag the value for jcz_product_picture.delete_flag
     *
     * @mbg.generated
     */
    public void setDeleteFlag(Byte deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}