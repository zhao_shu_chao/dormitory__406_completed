package com.yuanjun.product.model.bean;

public class TimeQuantum {
    private int type;

    private int interval;

    public TimeQuantum(int type, int interval) {
        this.type = type;
        this.interval = interval;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
