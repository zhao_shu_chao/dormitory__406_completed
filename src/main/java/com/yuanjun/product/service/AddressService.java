package com.yuanjun.product.service;

import com.yuanjun.product.entity.JczAddress;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.req.AddressReq;

import java.util.List;

public interface AddressService {
    //查询该用户所有收获地址
    List<AddressReq> findAddress(Integer userId);
    //新增用户收获地址
    String insertAddress(AddressReq addressReq);
    //修改时加载该地址信息
    JczAddress loadAddress(Integer id);
    //修改收货地址
    String updateAddress(AddressReq addressReq);
    //删除收货地址
    String deleteAddress(Integer id);
    //取消默认地址
    void updateAddressFlag(Integer id);
    //判断数据库中是否存在默认地址
    Integer checkDefaultFlag(Integer userId);

}
