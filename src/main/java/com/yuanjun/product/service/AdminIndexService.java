package com.yuanjun.product.service;

import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.resp.RespAdminIndexVo;

/**
 * 后台首页
 */
public interface AdminIndexService {
    RespAdminIndexVo adminIndex();
}
