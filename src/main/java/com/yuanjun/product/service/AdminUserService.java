package com.yuanjun.product.service;

import com.yuanjun.product.entity.JczSysAdmin;

public interface AdminUserService {
    JczSysAdmin getAdminUserByAccount(String account);
}
