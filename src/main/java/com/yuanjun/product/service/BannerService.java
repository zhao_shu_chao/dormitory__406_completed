package com.yuanjun.product.service;

import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.base.PageVo;
import com.yuanjun.product.vo.req.ReqBannerVo;

/**
 * @author zsc
 * @date 2020/2/4
 * TODO: 2020/2/4
 */
public interface BannerService {

    BaseResult addBanner(ReqBannerVo reqBannerVo);

    BaseResult updateBanner(ReqBannerVo reqBannerVo);

    BaseResult selectById(Integer id);

    PageVo selectBannerList(ReqBannerVo reqBannerVo);

    BaseResult deleteBanner(Integer id);

}
