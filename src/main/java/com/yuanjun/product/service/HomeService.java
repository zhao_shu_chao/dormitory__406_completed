package com.yuanjun.product.service;

import com.yuanjun.product.entity.JczUser;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.req.ReqIndexVo;

public interface HomeService {
    BaseResult index(JczUser user);
}
