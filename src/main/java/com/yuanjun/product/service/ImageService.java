package com.yuanjun.product.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author zsc
 * @date 2020/2/4
 * TODO: 2020/2/4
 */
public interface ImageService {
    String uploadImage(MultipartFile file, byte type);
}
