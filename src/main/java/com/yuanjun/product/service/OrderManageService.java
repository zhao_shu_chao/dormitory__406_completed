package com.yuanjun.product.service;

import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.base.PageVo;
import com.yuanjun.product.vo.req.ReqOrderManageVo;

public interface OrderManageService {
    /**
     * 后台订单管理
     * @param reqOrderManageVo
     * @return
     */
    PageVo orderManage(ReqOrderManageVo reqOrderManageVo);

    /**
     * 发货
     */
    BaseResult sendProduct(Integer orderId);
}
