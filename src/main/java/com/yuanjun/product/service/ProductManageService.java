package com.yuanjun.product.service;

import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.resp.RespUpdateProductLoadDateVo;

public interface ProductManageService {
    //商品列表

    //商品下架
    BaseResult underCarriage(Integer id);
    // 上架商品
    BaseResult shelvesProduct(Integer id);
    //加载商品信息
    BaseResult loadProduct(Integer id);

    BaseResult updateProductById(RespUpdateProductLoadDateVo respUpdateProductLoadDateVo) ;

}
