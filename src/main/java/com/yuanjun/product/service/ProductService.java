package com.yuanjun.product.service;

import com.yuanjun.product.entity.JczUser;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.req.*;

public interface ProductService {
    /**
     * 商品列表
     *
     * @param param
     * @return
     */
    BaseResult productList(ReqProductListVo param);

    /**
     * 商品详情
     *
     * @param param
     * @return
     */
    BaseResult productDetail(ReqProductDetailVo param);

    /**
     * 订单初始化
     *
     * @return
     */
    BaseResult makeOrder(JczUser user, ReqMakeOrderVo param);

    /**
     * 订单列表
     *
     * @param param
     * @return
     */
    BaseResult OrderList(ReqConfirmOrderVo param);

    /**
     * 进货单
     * @return
     */
    BaseResult ownOrderList(JczUser user, ReqOwnOrderListVo reqOwnOrderListVo);
    /**
     * 完成付款接口
     *
     * @param reqEndPaymentVo
     * @return
     */
    BaseResult endPayment(ReqEndPaymentVo reqEndPaymentVo);

    /**
     * 订单表存收货地址id
     * @param reqOrderAddress
     * @return
     */
    BaseResult orderAddress(ReqOrderAddress reqOrderAddress);
}
