package com.yuanjun.product.service;

import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.base.PageVo;
import com.yuanjun.product.vo.req.ReqAddProductVo;
import com.yuanjun.product.vo.req.ReqAdminProductListVo;

/**
 * @author: ZSC
 * @time: 2020/2/5
 */
public interface ReleaseProductService {
    /**
     * 发布商品
     * @return
     */
    BaseResult releaseProduct(ReqAddProductVo reqAddProductVo);

    PageVo adminProductList(ReqAdminProductListVo reqAdminProductListVo);

}
