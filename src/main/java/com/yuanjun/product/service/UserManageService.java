package com.yuanjun.product.service;

import com.yuanjun.product.vo.base.PageVo;
import com.yuanjun.product.vo.req.ReqUserManageVo;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
public interface UserManageService {

    /**
     * 显示用户列表
     * @return
     */
    PageVo userList(ReqUserManageVo reqUserManageVo);
}
