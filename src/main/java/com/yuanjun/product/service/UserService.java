package com.yuanjun.product.service;

import com.yuanjun.product.entity.JczSysAdmin;
import com.yuanjun.product.entity.JczUser;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.req.LoginReq;
import com.yuanjun.product.vo.req.RegisterReq;
import com.yuanjun.product.vo.req.ReqLoginVo;

public interface UserService {
    BaseResult register(RegisterReq registerReq);
    JczUser login(String phone);
    JczSysAdmin phoneLogin(String phone);
   JczUser findUserByPhone(String phone);
    boolean checkPhone(String phone);
    BaseResult modifyPassword(LoginReq loginReq);
    boolean isSpecialChar(String str);
}
