package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczAddressMapper;
import com.yuanjun.product.entity.JczAddress;
import com.yuanjun.product.entity.JczAddressExample;
import com.yuanjun.product.service.AddressService;
import com.yuanjun.product.vo.req.AddressReq;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AddressServiceImpl implements AddressService {
    private static final Integer DELETE_FLAG=0;
    @Resource
    private JczAddressMapper jczAddressMapper;


    @Override
    public List<AddressReq> findAddress(Integer userId) {
        JczAddressExample jczAddressExample = new JczAddressExample();
        jczAddressExample.createCriteria().andUserIdEqualTo(userId).andDeleteFlagEqualTo(DELETE_FLAG);
        List<JczAddress> jczAddressList=jczAddressMapper.selectByExample(jczAddressExample);
        List<AddressReq> respAddress=new ArrayList<>();
        for(JczAddress jczAddress:jczAddressList){
            AddressReq addressReq=new AddressReq();
            addressReq.setId(jczAddress.getId());
            addressReq.setUserId(jczAddress.getUserId());
            addressReq.setAddress(jczAddress.getAddress());
            addressReq.setDetailedAddress(jczAddress.getDetailedAddress());
            addressReq.setFlag(jczAddress.getDefaultFlag());
            addressReq.setPhone(jczAddress.getPhone());
            addressReq.setName(jczAddress.getConsignee());
            addressReq.setAddressName(jczAddress.getAddressName());
            respAddress.add(addressReq);
        }
        return respAddress;

    }

    @Override
    public String insertAddress(AddressReq addressReq) {
        Integer flag=checkDefaultFlag(addressReq.getUserId());
        if(flag!=0){
            updateAddressFlag(flag);
        }
        JczAddress jczAddress = new JczAddress();
        jczAddress.setUserId(addressReq.getUserId());//用户id
        jczAddress.setConsignee(addressReq.getName());//收货人姓名
        jczAddress.setPhone(addressReq.getPhone());//收货人联系方式
        jczAddress.setAddress(addressReq.getAddress());//地址信息
        jczAddress.setDetailedAddress(addressReq.getDetailedAddress());//详细地址
        jczAddress.setDefaultFlag(addressReq.getFlag());//是否是默认地址
        jczAddress.setAddressName(addressReq.getAddressName());
        int record=jczAddressMapper.insertSelective(jczAddress);
        if(record==0){
            return "添加失败";
        }
        return "添加成功";
    }

    @Override
    public JczAddress loadAddress(Integer id) {
        JczAddressExample jczAddressExample = new JczAddressExample();
        jczAddressExample.createCriteria().andIdEqualTo(id).andDeleteFlagEqualTo(0);
        return jczAddressMapper.selectByExample(jczAddressExample).get(0);
    }

    @Override
    public String updateAddress(AddressReq addressReq) {
        Integer flag=checkDefaultFlag(addressReq.getUserId());
        if(flag!=0){
            updateAddressFlag(flag);
        }
        JczAddress jczAddress =new JczAddress();
        jczAddress.setDetailedAddress(addressReq.getDetailedAddress());
        jczAddress.setAddress(addressReq.getAddress());
        jczAddress.setConsignee(addressReq.getName());
        jczAddress.setId(addressReq.getId());
        jczAddress.setPhone(addressReq.getPhone());
        jczAddress.setDefaultFlag(addressReq.getFlag());
        jczAddress.setUserId(addressReq.getUserId());
        jczAddress.setAddressName(addressReq.getAddressName());
        jczAddress.setCreateTime(new Date());
        jczAddress.setUpdateTime(new Date());
        jczAddress.setDeleteFlag(DELETE_FLAG);
        int record=jczAddressMapper.updateByPrimaryKey(jczAddress);
        if(record==0){
            return "修改失败";
        }
        return "修改成功";
    }

    @Override
    public String deleteAddress(Integer id) {
        int record=jczAddressMapper.deleteByPrimaryKey(id);
        if(record==0){
            return "删除失败";
        }
        return "删除成功";
    }

    @Override
    public void updateAddressFlag(Integer id) {
        JczAddressExample jczAddressExample=new JczAddressExample();
        jczAddressExample.createCriteria().andIdEqualTo(id).andDeleteFlagEqualTo(DELETE_FLAG);
        JczAddress jczAddress=jczAddressMapper.selectByExample(jczAddressExample).get(0);
        jczAddress.setDefaultFlag(0);
        jczAddressMapper.updateByPrimaryKeySelective(jczAddress);
    }

    /**
     * 判断是否存在默认地址
     * @return
     */
    @Override
    public Integer checkDefaultFlag(Integer userId) {
        JczAddressExample jczAddressExample=new JczAddressExample();
        jczAddressExample.createCriteria().andUserIdEqualTo(userId).andDefaultFlagEqualTo(1).andDeleteFlagEqualTo(DELETE_FLAG);
        List<JczAddress> jczAddressList=jczAddressMapper.selectByExample(jczAddressExample);
        if(jczAddressList.isEmpty()){
            return 0;
        }else{
            return jczAddressList.get(0).getId();
        }
    }


}
