package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczOrderMapper;
import com.yuanjun.product.entity.JczOrder;
import com.yuanjun.product.entity.JczOrderExample;
import com.yuanjun.product.service.AdminIndexService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.resp.RespAdminIndexVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: ZSC
 * @time: 2020/2/5
 *  总订单数  总成交额
 *  每日下单数  每日下单金额
 */
@Service
public class AdminIndexServiceImpl implements AdminIndexService {

    @Autowired
    private JczOrderMapper jczOrderMapper ;
    @Override
    public RespAdminIndexVo adminIndex() {
        JczOrderExample jczOrderExample = new JczOrderExample();
        jczOrderExample.createCriteria().andStatusBetween((byte)2,(byte)3);
        // 总订单数 只显示已经付款
        long allOrderCount = jczOrderMapper.countByExample(jczOrderExample);
        // 总成交额 只显示已经付款
        List<JczOrder> jczOrders = jczOrderMapper.selectByExample(jczOrderExample);
        BigDecimal allMoney = new BigDecimal(0);
        for (JczOrder jczOrder:jczOrders) {
          allMoney = allMoney.add(jczOrder.getTotalIncome());
        }
        // 当日成交数
        Long nowDayOrderCount = jczOrderMapper.selectNowDayOrderNumber();
        // 当日成交额
        List<JczOrder> nowDayOrder = jczOrderMapper.selectNowDayOrderAllMoney();
        BigDecimal nowDayMoney = new BigDecimal(0);
        if (nowDayOrder.size()>0 && null !=nowDayOrder){
            for (JczOrder jczOrder:nowDayOrder) {
                nowDayMoney = nowDayMoney.add(jczOrder.getTotalIncome());
            }
        }
        // 生成返回体
        RespAdminIndexVo respAdminIndexVo = new RespAdminIndexVo();
        respAdminIndexVo.setAllMoney(allMoney);
        respAdminIndexVo.setAllOrderCount(allOrderCount);
        respAdminIndexVo.setNowDayMoney(nowDayMoney);
        respAdminIndexVo.setNowDayOrderCount(nowDayOrderCount);

        return respAdminIndexVo;
    }

}
