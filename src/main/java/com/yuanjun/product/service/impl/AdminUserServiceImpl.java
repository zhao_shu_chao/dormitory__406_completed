package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczSysAdminMapper;
import com.yuanjun.product.entity.JczSysAdmin;
import com.yuanjun.product.entity.JczSysAdminExample;
import com.yuanjun.product.service.AdminUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdminUserServiceImpl implements AdminUserService {
    @Resource
    private JczSysAdminMapper jczSysAdminMapper;
    @Override
    public JczSysAdmin getAdminUserByAccount(String account) {
        JczSysAdminExample jczSysAdminExample = new JczSysAdminExample();
        jczSysAdminExample.createCriteria().andDeleteFlagEqualTo((byte)0).andPhoneEqualTo(account);
        List<JczSysAdmin> bhSysAdmins = jczSysAdminMapper.selectByExample(jczSysAdminExample);
        if (bhSysAdmins.isEmpty()){
            return null;
        }else{
            return bhSysAdmins.get(0);
        }
    }
}
