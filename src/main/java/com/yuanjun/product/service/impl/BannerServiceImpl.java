package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczBannerMapper;
import com.yuanjun.product.entity.JczBanner;
import com.yuanjun.product.entity.JczBannerExample;
import com.yuanjun.product.service.BannerService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.base.PageVo;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.ReqBannerVo;
import com.yuanjun.product.vo.resp.RespBannerAllListVo;
import com.yuanjun.product.vo.resp.RespBannerVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zsc
 * @date 2020/2/4
 * TODO: 2020/2/4
 */
@Service
public class BannerServiceImpl implements BannerService {
    @Resource
    private JczBannerMapper jczBannerMapper;

    @Override
    public BaseResult addBanner(ReqBannerVo reqBannerVo) {
        JczBannerExample jczBannerExample = new JczBannerExample();
        if (reqBannerVo.getBannerStatus() == 1){
            jczBannerExample.createCriteria().andDeleteFlagEqualTo((byte) 0).andBannerStatusEqualTo((byte) 0);
            List<JczBanner> jczBannerList = jczBannerMapper.selectByExample(jczBannerExample);
            if (jczBannerList.size() > 3){
                return BaseResult.createFail(Const.HttpRespCode.DATA_REPEAT,"显示轮播图超出数量限制");
            }
        }
        jczBannerExample.clear();
        jczBannerExample.createCriteria().andDeleteFlagEqualTo((byte) 0)
                .andLocationEqualTo(reqBannerVo.getLocation());
        List<JczBanner> jczBannerList = jczBannerMapper.selectByExample(jczBannerExample);
        if (!jczBannerList.isEmpty()) {
            return BaseResult.createFail(Const.HttpRespCode.DATA_REPEAT, "当前位置已存在轮播图");
        }
        JczBanner jczBanner = new JczBanner();
        BeanUtils.copyProperties(reqBannerVo,jczBanner);
        jczBanner.setCreateTime(new Date());
        jczBanner.setUpdateTime(jczBanner.getCreateTime());
        int rows = jczBannerMapper.insertSelective(jczBanner);
        if (rows == 0){
            return BaseResult.createFail(Const.HttpRespCode.INTERNAL_SERVER_ERROR,"添加失败");
        }
        return BaseResult.createOk(rows);
    }

    @Override
    public BaseResult updateBanner(ReqBannerVo reqBannerVo) {
        JczBannerExample jczBannerExample = new JczBannerExample();
        if (reqBannerVo.getBannerStatus() == 1){
            jczBannerExample.createCriteria().andDeleteFlagEqualTo((byte) 0).andBannerStatusEqualTo((byte) 0);
            List<JczBanner> jczBannerList = jczBannerMapper.selectByExample(jczBannerExample);
            if (jczBannerList.size() > 3){
                return BaseResult.createFail(Const.HttpRespCode.DATA_REPEAT,"显示轮播图超出数量限制");
            }
        }
        jczBannerExample.clear();
        jczBannerExample.createCriteria().andDeleteFlagEqualTo((byte) 0)
                .andLocationEqualTo(reqBannerVo.getLocation());
        List<JczBanner> jczBannerList = jczBannerMapper.selectByExample(jczBannerExample);
        if (jczBannerList.size() > 1) {
            return BaseResult.createFail(Const.HttpRespCode.DATA_REPEAT, "当前位置已存在轮播图");
        }
        JczBanner jczBanner = new JczBanner();
        BeanUtils.copyProperties(reqBannerVo,jczBanner);
        jczBanner.setUpdateTime(new Date());
        int rows = jczBannerMapper.updateByPrimaryKeySelective(jczBanner);
        if (rows == 0){
            return BaseResult.createFail(Const.HttpRespCode.INTERNAL_SERVER_ERROR,"修改失败");
        }
        return BaseResult.createOk(rows);
    }

    @Override
    public BaseResult selectById(Integer id) {
        JczBanner jczBanner = jczBannerMapper.selectByPrimaryKey(id);
        return BaseResult.createOk(jczBanner);
    }

    @Override
    public PageVo selectBannerList(ReqBannerVo reqBannerVo) {
        // 轮播图列表
        List<RespBannerAllListVo> respBannerVos = jczBannerMapper.selectBannerLists(reqBannerVo);
        // 轮播图总数
        Long count = jczBannerMapper.selectBannerListCount(reqBannerVo);
        //设置回参
        PageVo pageVo = new PageVo();
        pageVo.setList(respBannerVos);
        pageVo.setTotalCount(count);
        pageVo.setCurPage(reqBannerVo.getCurPage());
        pageVo.setPageSize(reqBannerVo.getPageSize());
        pageVo.setPageStart(reqBannerVo.getPageStart());
        return pageVo ;

    }

    @Override
    public BaseResult deleteBanner(Integer id) {
        JczBanner jczBanner = jczBannerMapper.selectByPrimaryKey(id);
        jczBanner.setDeleteFlag((byte) 1);
        jczBanner.setUpdateTime(new Date());
        int rows = jczBannerMapper.updateByPrimaryKeySelective(jczBanner);
        if (rows == 0){
            return BaseResult.createFail(Const.HttpRespCode.INTERNAL_SERVER_ERROR,"删除失败");
        }
        return BaseResult.createOk(rows);
    }
}
