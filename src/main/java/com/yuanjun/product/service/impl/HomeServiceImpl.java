package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczBannerMapper;
import com.yuanjun.product.dao.JczProductMapper;
import com.yuanjun.product.entity.JczBanner;
import com.yuanjun.product.entity.JczBannerExample;
import com.yuanjun.product.entity.JczUser;
import com.yuanjun.product.service.HomeService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.req.ReqIndexVo;
import com.yuanjun.product.vo.resp.RespIndexVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class HomeServiceImpl implements HomeService {
    @Resource
    private JczBannerMapper jczBannerMapper;
    @Resource
    private JczProductMapper jczProductMapper;

    @Override
    public BaseResult index(JczUser user) {
        // 获取banner图列表
        JczBannerExample bannerExample = new JczBannerExample();
        bannerExample.createCriteria().andDeleteFlagEqualTo((byte) 0).andBannerStatusEqualTo((byte) 0);
        bannerExample.setOrderByClause("'location' ASC");
        List<JczBanner> banners = jczBannerMapper.selectByExample(bannerExample);
        RespIndexVo respIndexVo = new RespIndexVo();
        respIndexVo.setBanners(banners);
        // 获取昵称
        if(Objects.isNull(user)){
            respIndexVo.setLogin(false);
        }else {
            respIndexVo.setLogin(true);
            respIndexVo.setUserName(user.getUserName());
        }
        return BaseResult.createOk(respIndexVo);
    }
}
