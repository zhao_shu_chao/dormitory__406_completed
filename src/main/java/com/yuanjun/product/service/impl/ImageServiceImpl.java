package com.yuanjun.product.service.impl;

import com.yuanjun.product.config.CCommonSettings;
import com.yuanjun.product.service.ImageService;
import com.yuanjun.product.utils.OssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Slf4j
@Service
public class ImageServiceImpl implements ImageService {

    @Override
    public String uploadImage(MultipartFile file, byte type) {
        //type 1 非加密的 2 加密
        if (file != null) {
            try {
                String fileUrl = "";
                if (type == 1) {
                    fileUrl = CCommonSettings.getProperty("file.url.public");
                } else if (type == 2) {
                    fileUrl = CCommonSettings.getProperty("file.url.private");
                }
                String url = fileUrl + OssUtil.uploadFile(file.getOriginalFilename(), "image", file.getInputStream(), type);
                return url;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
