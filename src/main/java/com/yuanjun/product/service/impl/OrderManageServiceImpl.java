package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczOrderMapper;
import com.yuanjun.product.entity.JczOrder;
import com.yuanjun.product.service.OrderManageService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.base.PageVo;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.ReqOrderManageVo;
import com.yuanjun.product.vo.resp.RespOrderDetailsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.yuanjun.product.vo.common.Const.HttpRespCode.INCOMPATIBLE_CONDITION;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Service
public class OrderManageServiceImpl implements OrderManageService {

    // 待发货状态
    public static final Byte WAITSENDPRODUCT = 2 ;
    // 已发货状态
    public static final Byte SENDPRODUCT = 3 ;
    @Autowired
    private JczOrderMapper jczOrderMapper ;

    /**
     * 订单管理
     */
    @Override
    public PageVo orderManage(ReqOrderManageVo reqOrderManageVo) {
        PageVo pageVO = new PageVo();
        // 只显示完成付款的订单
        List<RespOrderDetailsVo> respOrderDetailsVos = jczOrderMapper.selectOrderDetailList(reqOrderManageVo);
        Long count = jczOrderMapper.selectOrderDetailCount(reqOrderManageVo);
        pageVO.setList(respOrderDetailsVos);
        pageVO.setTotalCount(count);
        pageVO.setPageStart(reqOrderManageVo.getPageStart());
        pageVO.setCurPage(reqOrderManageVo.getCurPage());
        pageVO.setPageSize(reqOrderManageVo.getPageSize());
        return pageVO;
    }

    /**
     * 发货
     * @param orderId
     * @return
     */
    @Override
    public BaseResult sendProduct(Integer orderId) {
        JczOrder jczOrder = jczOrderMapper.selectByPrimaryKey(orderId);
        // 当订单状态不为代发货时
        if (WAITSENDPRODUCT !=jczOrder.getStatus()) {
          return BaseResult.createFail(INCOMPATIBLE_CONDITION,"该订单状态不支持此按钮");
        }
        JczOrder jczOrder1 = new JczOrder();
        jczOrder1.setId(jczOrder.getId());
        jczOrder1.setStatus(SENDPRODUCT);
        int i = jczOrderMapper.updateByPrimaryKeySelective(jczOrder1);
        JczOrder jczOrder2 = jczOrderMapper.selectByPrimaryKey(orderId);
       return  BaseResult.createOk(jczOrder2);
    }

}
