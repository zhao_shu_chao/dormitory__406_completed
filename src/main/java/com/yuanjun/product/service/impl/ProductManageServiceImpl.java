package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczProductMapper;
import com.yuanjun.product.dao.JczProductPictureMapper;
import com.yuanjun.product.entity.JczProduct;
import com.yuanjun.product.entity.JczProductExample;
import com.yuanjun.product.entity.JczProductPicture;
import com.yuanjun.product.entity.JczProductPictureExample;
import com.yuanjun.product.service.ProductManageService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.resp.RespUpdateProductLoadDateVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static com.yuanjun.product.service.impl.ReleaseProductServiceImpl.UNSHOW;
import static com.yuanjun.product.vo.common.Const.HttpRespCode.INCOMPATIBLE_CONDITION;

@Service
public class ProductManageServiceImpl implements ProductManageService {

    @Resource
    private JczProductMapper jczProductMapper;
    @Autowired
    private JczProductPictureMapper jczProductPictureMapper ;
    // 未删除
    private static final Integer UNDELETE_FLAG=0;
    //
    private static final Byte UNDER=1;

    /**
     * 商品下架
     * @param id
     * @return
     */
    @Override
    public BaseResult underCarriage(Integer id) {
        JczProductExample jczProductExample=new JczProductExample();
        jczProductExample.createCriteria().andIdEqualTo(id).andDeleteFalgEqualTo(UNDELETE_FLAG);
        JczProduct jczProduct=jczProductMapper.selectByExample(jczProductExample).get(0);
        jczProduct.setUpperShelfStatus(UNDER);
        jczProductMapper.updateByPrimaryKey(jczProduct);
        return BaseResult.createOk("商品已下架");
    }

    /**
     * 上架商品
     * @param id
     * @return
     */
    @Override
    public BaseResult shelvesProduct(Integer id) {
        JczProductExample jczProductExample=new JczProductExample();
        jczProductExample.createCriteria().andIdEqualTo(id).andDeleteFalgEqualTo(UNDELETE_FLAG);
        JczProduct jczProduct=jczProductMapper.selectByExample(jczProductExample).get(0);
        if (null == jczProduct){
                return BaseResult.createFail(INCOMPATIBLE_CONDITION,"参数有误，不存在该商品");
            }
        // 设置上架状态
        jczProduct.setUpperShelfStatus((byte)0);
        int i = jczProductMapper.updateByPrimaryKeySelective(jczProduct);
        if (1 != i){
            return BaseResult.createFail(INCOMPATIBLE_CONDITION,"操作有误");
        }
        return BaseResult.createOk("商品以上架",jczProduct);
    }

    /**
     * 加载商品信息
     * @param id
     * @return
     */
    @Override
    public BaseResult loadProduct(Integer id) {
        // 加载信息
        RespUpdateProductLoadDateVo respUpdateProductLoadDateVo = jczProductMapper.selectProductById(id);
        return BaseResult.createOk(respUpdateProductLoadDateVo) ;
    }


    /**
     * 修改商品
     * @param respUpdateProductLoadDateVo
     * @return
     */
    @Override
    public BaseResult updateProductById(RespUpdateProductLoadDateVo respUpdateProductLoadDateVo) {

        // 更新商品
        JczProduct jczProduct = new JczProduct();
        BeanUtils.copyProperties(respUpdateProductLoadDateVo,jczProduct);
        int i = jczProductMapper.updateByPrimaryKeySelective(jczProduct);


        // 更新图片
        List<String> pictureUrl = respUpdateProductLoadDateVo.getImgs();
        JczProductPictureExample jczProductPictureExample = new JczProductPictureExample();
        jczProductPictureExample.createCriteria().andProductIdEqualTo(respUpdateProductLoadDateVo.getId());
        // 删除原有图片（根据商品id）
        int i1 = jczProductPictureMapper.deleteByExample(jczProductPictureExample);
        // 插入商品图片库
        for(String img:pictureUrl){
            JczProductPicture jczProductPicture = new JczProductPicture();
            // 获取刚刚更新图片的id：jczProduct.getId()
            jczProductPicture.setProductId(jczProduct.getId());
            jczProductPicture.setPictureUrl(img);
            jczProductPictureMapper.insertSelective(jczProductPicture);
        }
        // 设置第一个插入的为订单显示图
        JczProductPictureExample jczProductPictureExample1 = new JczProductPictureExample();
        jczProductPictureExample1.createCriteria().andProductIdEqualTo(jczProduct.getId());
        List<JczProductPicture> jczProductPictures = jczProductPictureMapper.selectByExample(jczProductPictureExample1);
        JczProductPicture jczProductPicture = jczProductPictures.get(0);
        jczProductPicture.setIsShow(UNSHOW);
        int i11 = jczProductPictureMapper.updateByPrimaryKeySelective(jczProductPicture);
        if (i11 != 1){
            return BaseResult.createFail(INCOMPATIBLE_CONDITION,"设置主图有误");
        }
        return BaseResult.createOk(respUpdateProductLoadDateVo,"修改成功");
    }



}
