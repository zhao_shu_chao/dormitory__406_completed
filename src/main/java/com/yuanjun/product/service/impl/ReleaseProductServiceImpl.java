package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczProductMapper;
import com.yuanjun.product.dao.JczProductPictureMapper;
import com.yuanjun.product.entity.JczProduct;
import com.yuanjun.product.entity.JczProductPicture;
import com.yuanjun.product.entity.JczProductPictureExample;
import com.yuanjun.product.service.ReleaseProductService;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.base.PageVo;
import com.yuanjun.product.vo.req.ReqAddProductVo;
import com.yuanjun.product.vo.req.ReqAdminProductListVo;
import com.yuanjun.product.vo.resp.RespAdminProductListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import static com.yuanjun.product.vo.common.Const.HttpRespCode.INCOMPATIBLE_CONDITION;


/**
 * @author: ZSC
 * @time: 2020/2/5
 */
@Service
public class ReleaseProductServiceImpl implements ReleaseProductService {

    public static final Byte UNSHOW = 0;
    @Autowired
    private JczProductMapper jczProductMapper;
    @Autowired
    private JczProductPictureMapper jczProductPictureMapper;

    /**
     * 发布商品
     *
     * @param reqAddProductVo
     * @return
     */
    @Override
    public BaseResult releaseProduct(ReqAddProductVo reqAddProductVo) {
        JczProduct jczProduct = new JczProduct();
        BeanUtils.copyProperties(reqAddProductVo, jczProduct);
        // 插入商品表
        int i1 = jczProductMapper.insertByAddProduct(jczProduct);
        if (1 != i1) {
            return BaseResult.createFail(INCOMPATIBLE_CONDITION, "发布商品参时异常");
        }
        // 插入商品图片库
        List<String> imgs = reqAddProductVo.getImgs();
        for (String img : imgs) {
            JczProductPicture jczProductPicture = new JczProductPicture();
            // 获取刚刚插入的id：jczProduct.getId()
            jczProductPicture.setProductId(jczProduct.getId());
            jczProductPicture.setPictureUrl(img);
            jczProductPictureMapper.insertSelective(jczProductPicture);
        }
        // 设置第一个插入的为订单显示图
        JczProductPictureExample jczProductPictureExample1 = new JczProductPictureExample();
        jczProductPictureExample1.createCriteria().andProductIdEqualTo(jczProduct.getId());
        List<JczProductPicture> jczProductPictures = jczProductPictureMapper.selectByExample(jczProductPictureExample1);
        JczProductPicture jczProductPicture = jczProductPictures.get(0);
        jczProductPicture.setIsShow(UNSHOW);
        int i = jczProductPictureMapper.updateByPrimaryKeySelective(jczProductPicture);
        if (i != 1) {
            return BaseResult.createFail(INCOMPATIBLE_CONDITION, "设置主图有误");
        }
        return BaseResult.createOk(reqAddProductVo, "发布成功");
    }

    /**
     * 后台商品列表
     *
     * @param reqAdminProductListVo
     * @return
     */
    @Override
    public PageVo adminProductList(ReqAdminProductListVo reqAdminProductListVo) {
            List<RespAdminProductListVo> respAdminProductListVos = jczProductMapper.selectProductListByAdmin(reqAdminProductListVo);
            Long count = jczProductMapper.selectProductCountByAdmin(reqAdminProductListVo);
            PageVo pageVo = new PageVo();
            pageVo.setList(respAdminProductListVos);
            pageVo.setTotalCount(count);
            pageVo.setPageStart(reqAdminProductListVo.getPageStart());
            pageVo.setPageSize(reqAdminProductListVo.getPageSize());
            pageVo.setCurPage(reqAdminProductListVo.getCurPage());
            return pageVo;
            }
}