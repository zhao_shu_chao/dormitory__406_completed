package com.yuanjun.product.service.impl;

import com.yuanjun.product.dao.JczUserMapper;
import com.yuanjun.product.service.UserManageService;
import com.yuanjun.product.vo.base.PageVo;
import com.yuanjun.product.vo.req.ReqUserManageVo;
import com.yuanjun.product.vo.resp.RespUserManagementVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Service
public class UserManageServiceImpl implements UserManageService {

    @Autowired
    private JczUserMapper jczUserMapper;

    /**
     * 用户管理列表
     * @return
     */
    @Override
    public PageVo userList(@RequestBody ReqUserManageVo reqUserManageVo) {
        List<RespUserManagementVo> respUserManagementVos = jczUserMapper.selectUserList(reqUserManageVo);
        Long count = jczUserMapper.selectUserCount(reqUserManageVo);
        // 设置回参
        PageVo pageVo = new PageVo();
        pageVo.setList(respUserManagementVos);
        pageVo.setTotalCount(count);
        pageVo.setCurPage(reqUserManageVo.getCurPage());
        pageVo.setPageSize(reqUserManageVo.getPageSize());
        pageVo.setPageStart(reqUserManageVo.getPageStart());
        return pageVo;
    }
}
