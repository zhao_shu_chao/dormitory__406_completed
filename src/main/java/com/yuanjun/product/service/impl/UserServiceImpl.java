package com.yuanjun.product.service.impl;


import com.yuanjun.product.dao.JczSysAdminMapper;
import com.yuanjun.product.dao.JczUserMapper;
import com.yuanjun.product.entity.JczSysAdmin;
import com.yuanjun.product.entity.JczSysAdminExample;
import com.yuanjun.product.entity.JczUser;
import com.yuanjun.product.entity.JczUserExample;
import com.yuanjun.product.service.UserService;
import com.yuanjun.product.utils.MD5Util;
import com.yuanjun.product.vo.base.BaseResult;
import com.yuanjun.product.vo.common.Const;
import com.yuanjun.product.vo.req.LoginReq;
import com.yuanjun.product.vo.req.RegisterReq;
import com.yuanjun.product.vo.req.ReqLoginVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private JczUserMapper jczUserMapper;
    @Resource
    private JczSysAdminMapper jczSysAdminMapper;

    private static final Byte DELETE_FLAG=0;

    /**
     * 注册
     * @param registerReq
     * @return
     */
    @Override
    public BaseResult register(RegisterReq registerReq) {
        //对密码进行加密
        String password = MD5Util.encrypt(registerReq.getPassword());
        registerReq.setPassword(password);
        //插入user表信息
        JczUser jczUser=new JczUser();
        jczUser.setUserName(registerReq.getUserName());
        jczUser.setContactName(registerReq.getContactName());
        jczUser.setContactPhone(registerReq.getContactPhone());
        jczUser.setEmail(registerReq.getEmail());
        jczUserMapper.insertSelective(jczUser);
        //插入sysadmin表信息
        JczSysAdmin jczSysAdmin=new JczSysAdmin();
        jczSysAdmin.setAuthority(2);
        jczSysAdmin.setPassword(registerReq.getPassword());
        jczSysAdmin.setUserName(registerReq.getUserName());
        jczSysAdmin.setPhone(registerReq.getContactPhone());
        jczSysAdminMapper.insertSelective(jczSysAdmin);
        return BaseResult.createOk("注册成功");
    }

    /**
     * 手机号登录
     * @param phone
     * @return
     */
    @Override
    public JczUser login(String phone) {
        JczSysAdminExample jczSysAdminExample = new JczSysAdminExample();
        jczSysAdminExample.createCriteria().andPhoneEqualTo(phone).andAuthorityEqualTo(2).andDeleteFlagEqualTo(DELETE_FLAG);
        List<JczSysAdmin> jczSysAdmins = jczSysAdminMapper.selectByExample(jczSysAdminExample);
        if (jczSysAdmins.isEmpty()) {
            return null;
        } else {
            //返回user表对象
            JczUserExample jczUserExample = new JczUserExample();
            jczUserExample.createCriteria().andContactPhoneEqualTo(jczSysAdmins.get(0).getPhone()).andDeleteFlagEqualTo(DELETE_FLAG);
            return jczUserMapper.selectByExample(jczUserExample).get(0);
        }
    }

    /**
     * 手机号/用户名   密码登录
     * @param
     * @return
     */
    @Override
    public JczSysAdmin phoneLogin(String phone) {
        JczSysAdminExample jczSysAdminExample = new JczSysAdminExample();
        jczSysAdminExample.createCriteria().andPhoneEqualTo(phone).andAuthorityEqualTo(2).andDeleteFlagEqualTo(DELETE_FLAG);
        List<JczSysAdmin> jczSysAdmins = jczSysAdminMapper.selectByExample(jczSysAdminExample);
//        jczSysAdminExample.clear();
//        jczSysAdminExample.createCriteria().andUserNameEqualTo(phone).andAuthorityEqualTo(2).andDeleteFlagEqualTo(DELETE_FLAG);
//        List<JczSysAdmin> jczSysAdmins1 = jczSysAdminMapper.selectByExample(jczSysAdminExample);
        if (jczSysAdmins.isEmpty()){
            return null;
        }else{
//            JczSysAdmin jczSysAdmin=new JczSysAdmin();
//            if(jczSysAdmins.size()==0){
//                jczSysAdmin=jczSysAdmins1.get(0);
//            }else if(jczSysAdmins1.size()==0){
//                jczSysAdmin=jczSysAdmins.get(0);
//            }
            return jczSysAdmins.get(0);
        }
    }

    @Override
    public JczUser findUserByPhone(String phone){
        JczUserExample jczUserExample=new JczUserExample();
        jczUserExample.createCriteria().andContactPhoneEqualTo(phone).andDeleteFlagEqualTo(DELETE_FLAG);
        return jczUserMapper.selectByExample(jczUserExample).get(0);
    }

    /**
     * 手机号校验
     * @param phone
     * @return
     */
    public boolean checkPhone(String phone){
        //判断是否存在相同手机号
        boolean flag=false;
        JczSysAdminExample jczSysAdminExample = new JczSysAdminExample();
        jczSysAdminExample.createCriteria().andPhoneEqualTo(phone).andAuthorityEqualTo(2).andDeleteFlagEqualTo(DELETE_FLAG);
        List<JczSysAdmin> jczSysAdmins = jczSysAdminMapper.selectByExample(jczSysAdminExample);
        if(jczSysAdmins.isEmpty()) {
            flag=true;
        }
        return flag;
    }

    /**
     * 修改密码
     * @param
     */
    @Override
    public BaseResult modifyPassword(LoginReq loginReq) {
        JczSysAdminExample jczSysAdminExample=new JczSysAdminExample();
        jczSysAdminExample.createCriteria().andPhoneEqualTo(loginReq.getUserPhone()).andAuthorityEqualTo(2).andDeleteFlagEqualTo(DELETE_FLAG);
        List<JczSysAdmin> user=jczSysAdminMapper.selectByExample(jczSysAdminExample);
        user.get(0).setPassword(loginReq.getPassword());
        jczSysAdminMapper.updateByPrimaryKey(user.get(0));
        return BaseResult.createOk("修改成功");
    }

    /**
     * 判断密码中是否包含特殊字符
     * @param str
     * @return
     */
    @Override
    public boolean isSpecialChar(String str) {
        String regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.find();
    }

}
