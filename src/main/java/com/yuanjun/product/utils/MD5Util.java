package com.yuanjun.product.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
public class MD5Util {
    public static String encrypt(String dataStr) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(dataStr.getBytes(StandardCharsets.UTF_8));
            byte s[] = m.digest();
            String result = "";
            for (int i = 0; i < s.length; i++) {
                result += Integer.toHexString((0x000000FF & s[i]) | 0xFFFFFF00).substring(6);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }




    public static void main(String args[]){
      System.out.println(encrypt("12345"));
    }
}
