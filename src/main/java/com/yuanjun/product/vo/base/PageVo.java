package com.yuanjun.product.vo.base;

import java.io.Serializable;
import java.util.List;

public class PageVo<T> implements Serializable {

    /**
     * 第几页
     */
    private int curPage=1;

    /**
     * 查询起始点
     */
    private int pageStart;

    /**
     * 每页数量
     */
    private int pageSize=10;

    /**
     * 分页数据
     */
    private List<T> list;

    /**
     * 总数量
     */
    private long totalCount;

    /**
     * 总页数
     */
    private int pageCount;

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public int getPageStart() {
        return (this.curPage - 1) * this.pageSize;
    }

    public void setPageStart(int pageStart) {
        this.pageStart = pageStart;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageCount() {
        if (this.totalCount >= 0) {
            this.pageCount = (int) (totalCount / this.pageSize + ((totalCount % this.pageSize == 0) ? 0 : 1));
        }
        return pageCount;
    }
}
