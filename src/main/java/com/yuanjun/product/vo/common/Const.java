package com.yuanjun.product.vo.common;

public interface Const {
    String PROFRONTTOKEN = "jcz-front-token";
    String PROADMINTOKEN = "jcz-admin-token";
    String FRONT_PREFIX = "front";
    String ADMIN_PREFIX = "admin";
    String ORDER_PREFIX = "YJINS";

    interface HttpRespCode {
        int OK = 200;
        //请求的地址不存在或者包含不支持的参数
        int BAD_REQUEST = 400;
        //未授权
        int UNAUTHORIZED = 401;
        //未找到数据
        int NOT_FOUND = 404;
        //请求中数据缺失
        int DATA_LACK = 421;
        //token为空，或者失效
        int TOKEN_INVALID = 410;
        //不符合条件
        int INCOMPATIBLE_CONDITION = 405;
        //数据重复
        int DATA_REPEAT = 406;
        //服务异常
        int INTERNAL_SERVER_ERROR = 500;

    }
}

