package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class AddressReq {
    /**
     * 修改时传该地址id
     */
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 收货人姓名
     */
    private String name;
    /**
     * 收货人联系方式
     */
    private String phone;
    /**
     * 地址信息
     */
    private String address;
    /**
     * 详细地址
     */
    private String detailedAddress;
    /**
     * 是否是默认地址的标志位
     */
    private Integer flag;
    /**
     * 省市区名称
     */
    private String addressName;
}
