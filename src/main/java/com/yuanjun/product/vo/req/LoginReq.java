package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class LoginReq {
    /**
     * 手机号
     */
    private String userPhone;
    /**
     * 密码
     */
    private String password;
    /**
     * 验证码校验
     */
    private String captchaToken;
    /**
     * 短信验证码
     */
    private String randomCode;
    /**
     * 登录方式标志位
     */
    private Integer flag;
}
