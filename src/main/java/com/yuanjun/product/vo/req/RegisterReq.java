package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class RegisterReq {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 姓名
     */
    private String contactName;
    /**
     * 手机号
     */
    private String contactPhone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 验证码校验
     */
    String captchaToken;
    /**
     * 短信验证码
     */
    private String randomCode;
}
