package com.yuanjun.product.vo.req;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author: ZSC
 * @time: 2020/2/5
 */
@Data
public class ReqAddProductVo {

    // 产品id
    private Integer id ;

    // 商品名
    private String productName;

    // 品牌
    private String brand;

    // 单价
    private BigDecimal price;

    // 类别
    private String productType;

    // 描述
    private String describe ;

    // 图片地址
    private List<String> imgs ;

}
