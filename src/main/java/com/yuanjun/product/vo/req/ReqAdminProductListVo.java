package com.yuanjun.product.vo.req;

import com.yuanjun.product.vo.base.PageVo;
import lombok.Data;

/**
 * @author: ZSC
 * @time: 2020/2/6
 * 后台商品列表传入参数
 */
@Data
public class ReqAdminProductListVo extends PageVo {

    // 上下架状态
    private Integer upperShelfStatus ;

    // 商品名称
    private String productName ;


}
