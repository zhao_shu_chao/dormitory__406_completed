package com.yuanjun.product.vo.req;

import com.yuanjun.product.vo.base.PageVo;
import lombok.Data;

import java.util.Date;

@Data
public class ReqBannerVo extends PageVo {

    /**
     * 主键ID
     */
    private Integer id;

    /**
     *名称
     */
    private String bannerName;

    /**
     * 图片
     */
    private String bannerImageUrl;

    /**
     * 跳转地址
     */
    private String bannerJumpUrl;

    /**
     * 状态 0从首页下线 1添加至首页
     */
    private Byte bannerStatus;

    /**
     * 排序位置
     */
    private Integer location;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否删除 0正常 1删除
     */
    private Integer deleteFlag;
}