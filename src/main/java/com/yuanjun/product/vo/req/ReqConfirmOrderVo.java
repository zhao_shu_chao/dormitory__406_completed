package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class ReqConfirmOrderVo {
    private Integer orderId;
}
