package com.yuanjun.product.vo.req;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReqEndPaymentVo {
    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 支付方式
     */
    private Byte payType;

    /**
     * 实收款
     */
    private BigDecimal totalIncome;
}
