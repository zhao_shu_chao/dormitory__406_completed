package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class ReqLoginVo {
    /**
     * 账号
     */
    String account;
    /**
     * 密码
     */
    String password;
}
