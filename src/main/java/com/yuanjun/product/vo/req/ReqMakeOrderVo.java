package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class ReqMakeOrderVo {
    /**
     * 商品id
     */
    private Integer productId;
    /**
     * 购买数量
     */
    private Integer payCount;

}
