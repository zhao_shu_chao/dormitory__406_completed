package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class ReqOrderAddress {
    /**
     * 收货地址id
     */
    private Integer addressId;
    /**
     * 订单号
     */
    private Integer orderId;
}
