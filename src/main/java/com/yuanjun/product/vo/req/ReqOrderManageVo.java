package com.yuanjun.product.vo.req;

import com.yuanjun.product.vo.base.PageVo;
import lombok.Data;

import java.util.Date;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Data
public class ReqOrderManageVo extends PageVo {

    // 商品名称
    private String productName;

    // 下单开始时间
    private Date startTime;

    // 下单结束时间
    private Date endTime;

    // 订单编号
    private String orderNumber;

    // 订单状态
    private Integer status;


}
