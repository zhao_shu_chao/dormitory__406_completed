package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class ReqOwnOrderListVo {
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 当前页
     */
    private Integer curPage;
    /**
     * 订单id
     */
    private String orderId;
}
