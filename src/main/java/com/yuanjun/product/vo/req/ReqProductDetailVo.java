package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class ReqProductDetailVo {
    /**
     * 商品id
     */
    private Integer productId;
}
