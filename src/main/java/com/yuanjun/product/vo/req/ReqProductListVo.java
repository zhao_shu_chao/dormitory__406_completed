package com.yuanjun.product.vo.req;

import lombok.Data;

@Data
public class ReqProductListVo {
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 当前页
     */
    private Integer curPage;
    /**
     * 产品名称
     */
    private String productName;
}
