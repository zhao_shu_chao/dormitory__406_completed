package com.yuanjun.product.vo.req;

import com.yuanjun.product.vo.base.PageVo;
import lombok.Data;


/**
 * @author: ZSC
 * @time: 2020/2/5
 */
@Data
public class ReqUserManageVo extends PageVo {

    /**
     * 用户昵称
     */
    private String userName;
    /**
     * 手机号码
     */
    private String phone;


}
