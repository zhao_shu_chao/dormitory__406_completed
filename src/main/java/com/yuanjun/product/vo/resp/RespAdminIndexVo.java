package com.yuanjun.product.vo.resp;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: ZSC
 * @time: 2020/2/5
 */
@Data
public class RespAdminIndexVo {

    // 总订单数
    private Long allOrderCount ;

    // 总成交额
    private BigDecimal allMoney ;

    // 当日订单数
    private Long nowDayOrderCount ;

    // 当日成交金额
    private BigDecimal  nowDayMoney ;
}
