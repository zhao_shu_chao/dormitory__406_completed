package com.yuanjun.product.vo.resp;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: ZSC
 * @time: 2020/2/6
 * 后台商品列表回参
 */
@Data
public class RespAdminProductListVo {

    private Integer id;
    // 商品名称
    private String ProductName;

    // 商品图片地址
    private String pictureUrl;

    // 商品描述
    private String describe;

    // 商品价格
    private BigDecimal price ;

    private Integer upperShelfStatus ;

    // 该商品总销量
    private Long sales;
}
