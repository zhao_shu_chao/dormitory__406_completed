package com.yuanjun.product.vo.resp;

import lombok.Data;

import java.util.Date;

/**
 * @author: ZSC
 * @time: 2020/3/11
 */
@Data
public class RespBannerAllListVo {

    private Integer id;

    private String bannerName;

    private String bannerImageUrl;

    private String bannerJumpUrl;

    private Byte bannerStatus;

    private Integer location;




}
