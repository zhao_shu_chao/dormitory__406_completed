package com.yuanjun.product.vo.resp;

import com.yuanjun.product.vo.req.ReqBannerVo;
import lombok.Data;

import java.util.List;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Data
public class RespBannerVo {

    /**
     * banner列表
     */
    private List<ReqBannerVo> bannerVoList;

    /**
     * 轮播图总数
     */
    private Integer bannerCount;

    /**
     * 首页轮播图总数
     */
    private Integer indexBannerCount;
}
