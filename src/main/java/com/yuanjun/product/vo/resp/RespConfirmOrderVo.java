package com.yuanjun.product.vo.resp;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RespConfirmOrderVo {
    private Integer id;
    private String productName;
    private String describe;
    private String pictureUrl;
    private BigDecimal price;
    private Integer totalCount;
    private BigDecimal totalIncome;
}
