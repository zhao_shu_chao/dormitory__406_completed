package com.yuanjun.product.vo.resp;

import com.yuanjun.product.entity.JczBanner;
import lombok.Data;

import java.util.List;

@Data
public class RespIndexVo {
    /**
     * banner图列表
     */
    private List<JczBanner> banners;
    /**
     * 用户名
     */
    private String userName;

    /**
     * 是否登录
     */
    private boolean login ;
}
