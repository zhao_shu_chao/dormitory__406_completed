package com.yuanjun.product.vo.resp;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Data
public class RespOrderDetailsVo {

        private Integer id;

        // 订单编号
        private String orderNumber;

        // 下单时间
        private Date createTime;

        // 买家姓名
        private String userName;

        // 买家手机号
        private String phone;

        // 收货地址
        private String address;

        // 详细地址
        private String detailedAddress;

        // 商品名称
        private String productName;

        // 商品数量
        private Integer count;

        // 商品图片
        private String pictureUrl;

        // 商品描述
        private String proDescribe;

        // 交易状态
        private Integer status;

        // 总金额
        private BigDecimal income;
}
