package com.yuanjun.product.vo.resp;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Data
public class RespOwnOrderListVo {
    private Integer id;
    private String orderNumber;
    private BigDecimal price;
    private Integer totalCount;
    private BigDecimal totalIncome;
    private String pictureUrl;
    private String productName;
    private String describe;
    private String status;
    private Date payTime;
    private String address;
    private String detailedAddress;
    private String addressName;
    private String contactName;
    private String contactPhone;
}
