package com.yuanjun.product.vo.resp;

import com.yuanjun.product.entity.JczProduct;
import lombok.Data;

import java.util.List;
/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Data
public class RespProductDetailVo extends JczProduct {
    private List<String> pictureUrl;
}
