package com.yuanjun.product.vo.resp;

import lombok.Data;

import java.math.BigDecimal;
/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Data
public class RespProductListVo {
    /**
     * id
     */
    private Integer id;
    /**
     * 图片url
     */
    private String pictureUrl;
    /**
     * 单价
     */
    private BigDecimal price;
    /**
     * 产品描述
     */
    private String describe;
    /**
     * 购买次数
     */
    private Long payCount;
}
