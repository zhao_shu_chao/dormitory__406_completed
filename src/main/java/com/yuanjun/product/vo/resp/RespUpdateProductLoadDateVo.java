package com.yuanjun.product.vo.resp;

import lombok.Data;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author: ZSC
 * @time: 2020/2/6
 */
@Data
public class RespUpdateProductLoadDateVo {

    // id
    private Integer id;

     //商品名称
    private String productName;

    // 描述
    private String describe;

    // 单价
    private BigDecimal price;

    // 品牌
    private String brand;

    // 类别
    private String productType;

    // 图片
    List<String> imgs ;

}
