package com.yuanjun.product.vo.resp;

import lombok.Data;

import java.util.Date;

/**
 * @author: ZSC
 * @time: 2020/2/4
 */
@Data
public class RespUserManagementVo {

    private Integer id ;
    // 用户昵称
    private String userName;

    // 手机号
    private String phone;

    // 注册时间
    private Date createTime;

}
